//
//  LoginVC.m
//  PingPongApp
//
//  Created by keval vasaniya on 31/07/17.
//  Copyright © 2017 keval vasaniya. All rights reserved.
//

#import "LoginVC.h"
#import "SidebarVC.h"
#import "DYQRCodeDecoderViewController.h"
#import "User.h"
#import "DefaultsValues.h"

@interface LoginVC ()
{
    BOOL isTap;
    BOOL isSuccess;
    NSString * readResult;
}
@end

@implementation LoginVC
@synthesize txtUserName, txtPassword, btnLogin, lblLoginWithQr;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTextfiledBottomLine:txtUserName:@"Username":@"username"];
    [self setTextfiledBottomLine:txtPassword:@"Password":@"password" ];
    
    [self setButtonRound:btnLogin];
    
    lblLoginWithQr.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapLabelWithGesture:)];
    [lblLoginWithQr addGestureRecognizer:tapGesture];
    isTap = false;
    isSuccess = false;
    readResult=@"";
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    if(isTap){
        if(isSuccess){
            
            /*UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Info" message:[NSString stringWithFormat:@"Scaner have read '%@' from barcode. But app don't allow to login because it want some detail of user to fetch data of matches, So you will get it working from next demo after discussion with Vimal. Please use the normal login for now", readResult] preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:ok];
            [self presentViewController:alertController animated:YES completion:nil];*/
        }else{
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Info" message:[NSString stringWithFormat:@"Failed to scan barcode"] preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:ok];
            [self presentViewController:alertController animated:YES completion:nil];
        }
        isTap = false;
    }
}

- (void)didTapLabelWithGesture:(UITapGestureRecognizer *)tapGesture {
    isTap = true;
    DYQRCodeDecoderViewController *vc = [[DYQRCodeDecoderViewController alloc] initWithCompletion:^(BOOL succeeded, NSString *result) {
        if (succeeded) {
            isSuccess = true;
            readResult =result;
            NSLog(@"Success! %@", result);
            NSData *data = [result dataUsingEncoding:NSUTF8StringEncoding];
            id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
            NSLog(@"Jsooooooooooooooooooommmmmmmmmmmmmmmmm %@",json);
            NSLog(@"%@",[[[json valueForKey:@"User"] objectAtIndex:0]valueForKey:@"id"]);
            
            if([json valueForKey:SUCCESS_STATUS] > 0){
                
                //User *object_user = [[json valueForKey:@"User"] objectAtIndex:0];
                
                [DefaultsValues setStringValueToUserDefaults:[[[json valueForKey:@"User"] objectAtIndex:0] valueForKey:@"id"] ForKey:kUserID];
                [DefaultsValues setStringValueToUserDefaults:[[[json valueForKey:@"User"] objectAtIndex:0] valueForKey:@"club_id"] ForKey:kClubID];
                [DefaultsValues setStringValueToUserDefaults:[[[json valueForKey:@"User"] objectAtIndex:0] valueForKey:@"firstname"] ForKey:kFirstName];
                [DefaultsValues setStringValueToUserDefaults:[[[json valueForKey:@"User"] objectAtIndex:0] valueForKey:@"lastname"] ForKey:kLastName];
                [DefaultsValues setStringValueToUserDefaults:[[[json valueForKey:@"User"] objectAtIndex:0] valueForKey:@"photo"] ForKey:kPhoto];
                [DefaultsValues setStringValueToUserDefaults:[[[json valueForKey:@"User"] objectAtIndex:0] valueForKey:@"photo"] ForKey:kclub_logo];
                
                HomeVC *main = loadViewController(StoryBoard_Home, kIDHomeVC);
                SidebarVC *drawer = loadViewController(StoryBoard_Home, kIDSidebarVC);
                self.centerNavigationController = [[UINavigationController alloc] initWithRootViewController:main];
                
                self.leftNavigationController = [[UINavigationController alloc] initWithRootViewController:drawer];
                self.drawerController = [[MMDrawerController alloc] initWithCenterViewController:self.centerNavigationController leftDrawerViewController:self.leftNavigationController rightDrawerViewController:nil];
                [self.drawerController setMaximumLeftDrawerWidth:220];
                [self.drawerController setShowsShadow:NO];
                [self.drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeNone];
                [self.drawerController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
                //self.navigationController.navigationBar.backgroundColor = [UIColor whiteColor];
                //[self.navigationController pushViewController:self.drawerController animated:YES];
                
                
                [self presentViewController:self.drawerController
                                   animated:YES
                                 completion:^{
                                     
                                     
                                 }];
            }else{
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Alert" message:[json valueForKey:@"messsage"] preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alertController addAction:ok];
                [self presentViewController:alertController animated:YES completion:nil];
                
            }
            
        } else {
            isSuccess = false;
            readResult = @"";
            NSLog(@"Failed!");
            
        }
    }];
    [self presentViewController:[[UINavigationController alloc] initWithRootViewController:vc] animated:YES completion:NULL];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

/*
 
 */

#pragma mark - Button click event

- (IBAction)btnLoginClick:(id)sender {
    
    if([txtUserName.text length] > 0 && [txtPassword.text length] > 0){
        [self getLogin:@"keval" password:@"winwincoco"];
    }
    else{
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Alert" message:@"Invalid username or password!" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        [self presentViewController:alertController animated:YES completion:nil];
    
    }
}


- (void)getLogin:(NSString *)username password:(NSString *)password
{
    [SVProgressHUD showWithStatus:@"Loading Details"];
    [[WebServiceConnector alloc]init:[NSString stringWithFormat:@"%@username=%@&password=%@",URL_Login,txtUserName.text,txtPassword.text]
                      withParameters:@{MemberId : @"39"}
                          withObject:self
                        withSelector:@selector(displayLoginResponse:)
                      forServiceType:@"POST"
                      showDisplayMsg:@""
                showNetworkIndicator:NO];
}


- (IBAction)displayLoginResponse:(id)sender
{
    if([[sender responseDict] valueForKey:SUCCESS_STATUS] > 0){
        
        User *object_user = [[sender responseArray] objectAtIndex:0];
        
        [DefaultsValues setStringValueToUserDefaults:object_user.userIdentifier ForKey:kUserID];
        [DefaultsValues setStringValueToUserDefaults:object_user.clubId ForKey:kClubID];
        [DefaultsValues setStringValueToUserDefaults:object_user.firstname ForKey:kFirstName];
        [DefaultsValues setStringValueToUserDefaults:object_user.lastname ForKey:kLastName];
        [DefaultsValues setStringValueToUserDefaults:object_user.photo ForKey:kPhoto];
        [DefaultsValues setStringValueToUserDefaults:object_user.clubLogo ForKey:kclub_logo];
        
        HomeVC *main = loadViewController(StoryBoard_Home, kIDHomeVC);
        SidebarVC *drawer = loadViewController(StoryBoard_Home, kIDSidebarVC);
        self.centerNavigationController = [[UINavigationController alloc] initWithRootViewController:main];
        
        self.leftNavigationController = [[UINavigationController alloc] initWithRootViewController:drawer];
        self.drawerController = [[MMDrawerController alloc] initWithCenterViewController:self.centerNavigationController leftDrawerViewController:self.leftNavigationController rightDrawerViewController:nil];
        [self.drawerController setMaximumLeftDrawerWidth:220];
        [self.drawerController setShowsShadow:NO];
        [self.drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeNone];
        [self.drawerController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
        [self.drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
        //self.navigationController.navigationBar.backgroundColor = [UIColor whiteColor];
        //[self.navigationController pushViewController:self.drawerController animated:YES];
        
        
        [self presentViewController:self.drawerController
                           animated:YES
                         completion:^{
                             
              
                         }];
    }else{
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Alert" message:[sender responseError] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        [self presentViewController:alertController animated:YES completion:nil];
        
    }
    
   
    
    
}
@end
