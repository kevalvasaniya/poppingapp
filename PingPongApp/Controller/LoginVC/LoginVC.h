//
//  LoginVC.h
//  PingPongApp
//
//  Created by keval vasaniya on 31/07/17.
//  Copyright © 2017 keval vasaniya. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseVC.h"
#import "HomeVC.h"
#import <QuartzCore/QuartzCore.h>

@interface LoginVC : BaseVC
- (IBAction)btnLoginClick:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;


@property (weak, nonatomic) IBOutlet UITextField *txtUserName;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;

@property (weak, nonatomic) IBOutlet UILabel *lblLoginWithQr;
@end
