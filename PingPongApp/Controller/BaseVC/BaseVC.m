//
//  BaseVC.m
//  PingPongApp
//
//  Created by keval vasaniya on 31/07/17.
//  Copyright © 2017 keval vasaniya. All rights reserved.
//

#import "BaseVC.h"
#import "HomeVC.h"
#import "SidebarVC.h"

@interface BaseVC ()

@end

@implementation BaseVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Set Drawer
-(void) setInitialViews
{
    // set view controllers
    HomeVC *main = loadViewController(StoryBoard_Home, kIDHomeVC);
    SidebarVC *drawer = loadViewController(StoryBoard_Home, kIDSidebarVC);
    self.centerNavigationController = [[UINavigationController alloc] initWithRootViewController:main];
    self.leftNavigationController = [[UINavigationController alloc] initWithRootViewController:drawer];
    self.drawerController = [[MMDrawerController alloc] initWithCenterViewController:self.centerNavigationController leftDrawerViewController:self.leftNavigationController rightDrawerViewController:nil];
    [self.drawerController setMaximumLeftDrawerWidth:220];
    [self.drawerController setShowsShadow:NO];
    [self.drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeNone];
    [self.drawerController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
    self.navigationController.navigationBar.backgroundColor = [UIColor whiteColor];
    [self.navigationController pushViewController:self.drawerController animated:YES];
}

-(void)setTextfiledBottomLine : (UITextField *)txtField : (NSString *)placeholder : (NSString *)imageName
{
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 1;
    border.borderColor = [[UIColor colorWithRed:62.0f/255.0f green:138.0f/255.0f blue:201.0f/255.0f alpha:1.0] CGColor];
    
    border.frame = CGRectMake(0, txtField.frame.size.height - borderWidth, txtField.frame.size.width, txtField.frame.size.height + 2);
    border.borderWidth = borderWidth;
    [txtField.layer addSublayer:border];
    txtField.layer.masksToBounds = YES;
    txtField.attributedPlaceholder =
    [[NSAttributedString alloc]
     initWithString:placeholder
     attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    
    UIImageView *img = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
    img.frame = CGRectMake(0.0, -2, img.image.size.width, img.image.size.height);
    img.contentMode = UIViewContentModeCenter;
    
    
    txtField.leftViewMode = UITextFieldViewModeAlways;
    txtField.leftView = img;
}

-(void)setButtonRound:(UIButton *)button
{
    button.layer.cornerRadius = 20; // this value vary as per your desire
    button.clipsToBounds = YES;
    button.layer.borderColor = [[UIColor colorWithRed:62.0f/255.0f green:138.0f/255.0f blue:201.0f/255.0f alpha:1.0] CGColor];
    [button.layer setBorderWidth:1.0];
}

#pragma mark - Textbox border
-(void)setTextFeildBorder:(UITextField *)textfield borderColor:(NSString *)hexString{
    textfield.layer.borderWidth = 1;
    textfield.layer.borderColor = [[self colorWithHexString:hexString alpha:1] CGColor];
}

-(void)setTextViewBorder:(UITextView *)textfield borderColor:(NSString *)hexStrin{
    textfield.layer.borderWidth = .5f;
    textfield.layer.borderColor = [[self colorWithHexString:hexStrin alpha:1] CGColor];
}

#pragma mark - Hext to color
- (UIColor *)colorWithHexString:(NSString *)str_HEX  alpha:(CGFloat)alpha_range{
    int red = 0;
    int green = 0;
    int blue = 0;
    sscanf([str_HEX UTF8String], "#%02X%02X%02X", &red, &green, &blue);
    return  [UIColor colorWithRed:red/255.0 green:green/255.0 blue:blue/255.0 alpha:alpha_range];
}

#pragma mark - Navigation bar setting
-(void)navigationSetting{
    [ self.navigationController.navigationBar setBarTintColor :
     [self colorWithHexString:@"#1A2B3A" alpha:1] ];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
}

#pragma mark - Split string
-(NSArray *)splitString:(NSString *)str splitBy:(NSString *)split{
    NSArray *listItems = [str componentsSeparatedByString:split];
    return listItems;
}


@end
