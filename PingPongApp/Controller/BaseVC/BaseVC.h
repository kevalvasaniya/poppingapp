//
//  BaseVC.h
//  PingPongApp
//
//  Created by keval vasaniya on 31/07/17.
//  Copyright © 2017 keval vasaniya. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"
#import "NavigationVC.h"
#import "UIViewController+helper.h"
#import "MMDrawerController.h"
#import "SVProgressHUD.h"
#import "WebServiceDataAdaptor.h"
#import "WebServiceConnector.h"
#import "WSConstant.h"

@interface BaseVC : UIViewController
@property (strong, nonatomic) MMDrawerController *drawerController;
@property (strong, nonatomic) UINavigationController *centerNavigationController;
@property (strong, nonatomic) UINavigationController *leftNavigationController;
-(void) setInitialViews;
-(void)setTextfiledBottomLine : (UITextField *)txtField : (NSString *)placeholder : (NSString *)imageName;
-(void)setButtonRound:(UIButton *)button;
- (UIColor *)colorWithHexString:(NSString *)str_HEX  alpha:(CGFloat)alpha_range;
-(void)setTextFeildBorder:(UITextField *)textfield borderColor:(NSString *)hexString;
-(void)setTextViewBorder:(UITextView *)textfield borderColor:(NSString *)hexString;
-(void)navigationSetting;
-(NSArray *)splitString:(NSString *)str splitBy:(NSString *)split;
@end
