//
//  SidebarVC.m
//  PingPongApp
//
//  Created by keval vasaniya on 31/07/17.
//  Copyright © 2017 keval vasaniya. All rights reserved.
//

#import "SidebarVC.h"
#import "UIViewController+MMDrawerController.h"
#import "WebServiceConnector.h"
#import "WSConstant.h"
#import "SVProgressHUD.h"
#import "List.h"
#import "Child.h"
#import "SudeMenuTVCHeader.h"
#import "AddMatchesVC.h"
#import "AllClubMatchesVC.h"
#import "MyMatchesVC.h"
#import "DefaultsValues.h"
#import "NSData+ImageContentType.h"
#import <ImageIO/ImageIO.h>
#import "UIImageView+WebCache.h"
#import "DYQRCodeDecoderViewController.h"
#import "HomeVC.h"
//#import <SDWebImage/UIImageView+WebCache.h>

#define count1 12
@interface SidebarVC ()
{
    NSMutableArray *arrMenuItems ;
    NSMutableArray *arrSelectedSectionIndex;
    BOOL isMultipleExpansionAllowed;
    NSMutableArray *arriCon;
    DYQRCodeDecoderViewController *vc;
}
@end

@implementation SidebarVC
@synthesize tblMenu;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
    tblMenu.backgroundColor = [UIColor clearColor];
    arrMenuItems = [[NSMutableArray alloc]init];
    arriCon = [[NSMutableArray alloc]initWithObjects:@"home_icon",@"Internammatches_icon",@"InternalRating",@"tournaments_icon",@"EmailManagement_icon",@"Canteen_icon",@"Meeting_icon",@"event_icon",@"stats_icon",@"ourclub_icon",@"setting_icon", nil];
    
    isMultipleExpansionAllowed = NO;
    
    arrSelectedSectionIndex = [[NSMutableArray alloc] init];
    
    if (!isMultipleExpansionAllowed) {
        [arrSelectedSectionIndex addObject:[NSNumber numberWithInt:count1+2]];
    }
    [self getLestMenu];
    
    _lblUsetName.text = [NSString stringWithFormat:@
                         "%@ %@",[DefaultsValues getStringValueFromUserDefaults_ForKey:kFirstName],[DefaultsValues getStringValueFromUserDefaults_ForKey:kLastName]];
    
    //[_imgUserPic sd_setImageWithURL:[NSURL URLWithString:[DefaultsValues getStringValueFromUserDefaults_ForKey:kPhoto]] placeholderImage:[UIImage imageNamed:@"AddMatch"]];
    [_imgUserPic sd_setImageWithURL:[NSURL URLWithString:[DefaultsValues getStringValueFromUserDefaults_ForKey:kPhoto]] placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    //User Profile image
    //_imgUserPic.frame = CGRectMake(self.lblUserName.frame.size.width / 2 - 40, self.lblUserName.frame.origin.y -100, 100 , 100);
    _imgUserPic.layer.cornerRadius = _imgUserPic.frame.size.width / 2;
    _imgUserPic.clipsToBounds=YES;
    _imgUserPic.layer.borderColor = [UIColor colorWithRed:188/255.0 green:188/255.0 blue:188/255.0 alpha:1].CGColor;
    _imgUserPic.layer.borderWidth = 5.0;
    
    _imgClubLogo.layer.cornerRadius = _imgClubLogo.frame.size.width / 2;
    _imgClubLogo.clipsToBounds=YES;
    
    NSLog(@"%@",[DefaultsValues getStringValueFromUserDefaults_ForKey:kclub_logo]);
    [_imgClubLogo sd_setImageWithURL:[NSURL URLWithString:[DefaultsValues getStringValueFromUserDefaults_ForKey:kclub_logo]] placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    
    
    
   vc = [[DYQRCodeDecoderViewController alloc] initWithCompletion:^(BOOL succeeded, NSString *result) {
        if (succeeded) {
            //isSuccess = true;
            //readResult =result;
            NSLog(@"Success! %@", result);
            NSData *data = [result dataUsingEncoding:NSUTF8StringEncoding];
            id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
            NSLog(@"Jsooooooooooooooooooommmmmmmmmmmmmmmmm %@",json);
            NSLog(@"%@",[[[json valueForKey:@"User"] objectAtIndex:0]valueForKey:@"id"]);
            
            if([json valueForKey:SUCCESS_STATUS] > 0){
                
                //User *object_user = [[json valueForKey:@"User"] objectAtIndex:0];
                
                [DefaultsValues setStringValueToUserDefaults:[[[json valueForKey:@"User"] objectAtIndex:0] valueForKey:@"id"] ForKey:kUserID];
                [DefaultsValues setStringValueToUserDefaults:[[[json valueForKey:@"User"] objectAtIndex:0] valueForKey:@"club_id"] ForKey:kClubID];
                [DefaultsValues setStringValueToUserDefaults:[[[json valueForKey:@"User"] objectAtIndex:0] valueForKey:@"firstname"] ForKey:kFirstName];
                [DefaultsValues setStringValueToUserDefaults:[[[json valueForKey:@"User"] objectAtIndex:0] valueForKey:@"lastname"] ForKey:kLastName];
                [DefaultsValues setStringValueToUserDefaults:[[[json valueForKey:@"User"] objectAtIndex:0] valueForKey:@"photo"] ForKey:kPhoto];
                [DefaultsValues setStringValueToUserDefaults:[[[json valueForKey:@"User"] objectAtIndex:0] valueForKey:@"photo"] ForKey:kclub_logo];
                
                HomeVC *main = loadViewController(StoryBoard_Home, kIDHomeVC);
                SidebarVC *drawer = loadViewController(StoryBoard_Home, kIDSidebarVC);
                self.centerNavigationController = [[UINavigationController alloc] initWithRootViewController:main];
                
                self.leftNavigationController = [[UINavigationController alloc] initWithRootViewController:drawer];
                self.drawerController = [[MMDrawerController alloc] initWithCenterViewController:self.centerNavigationController leftDrawerViewController:self.leftNavigationController rightDrawerViewController:nil];
                [self.drawerController setMaximumLeftDrawerWidth:220];
                [self.drawerController setShowsShadow:NO];
                [self.drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeNone];
                [self.drawerController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
                //self.navigationController.navigationBar.backgroundColor = [UIColor whiteColor];
                //[self.navigationController pushViewController:self.drawerController animated:YES];
                
                
                [self presentViewController:self.drawerController
                                   animated:YES
                                 completion:^{
                                     
                                     
                                 }];
            }else{
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Alert" message:[json valueForKey:@"messsage"] preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alertController addAction:ok];
                [self presentViewController:alertController animated:YES completion:nil];
                
            }
            
        } else {
            
            NSLog(@"Failed!");
            
        }
    }];
}

- (void)getLestMenu
{
    [SVProgressHUD showWithStatus:@"Loading Details"];
    [[WebServiceConnector alloc]init:[NSString stringWithFormat:@"%@%@=%@",URL_LeftMenu,MemberId,[DefaultsValues getStringValueFromUserDefaults_ForKey:kUserID]]
                      withParameters:@{MemberId : @"39"}
                          withObject:self
                        withSelector:@selector(displayLeftMenuResponse:)
                      forServiceType:@"POST"
                      showDisplayMsg:@""
                showNetworkIndicator:NO];
}

- (IBAction)displayLeftMenuResponse:(id)sender
{
    
    arrMenuItems =  [[sender responseDict] valueForKey:@"List"];
    NSLog(@"$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
    NSLog(@"%@",[[[sender responseDict] valueForKey:@"List"] valueForKey:@"main"]);
    NSLog(@"$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
    [tblMenu reloadData];
    
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark - Table view delegate methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //NSLog(@"%lu",(unsigned long)[arrMenuItems count]);
    return [arrMenuItems count];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    List *obj_list = [arrMenuItems objectAtIndex:section];
    //NSLog(@"%lu",[[obj_list valueForKey:@"child"] count]);
    
    if ([arrSelectedSectionIndex containsObject:[NSNumber numberWithInteger:section]])
    {
        return [[obj_list valueForKey:@"child"] count];
    }else{
        return 0;
    }
    
}



-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"idCell";
    
    SideMenuTVC *cell = [tblMenu dequeueReusableCellWithIdentifier:cellIdentifier];
    
    
    if (cell == nil) {
        cell = [[SideMenuTVC alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.backgroundColor = [UIColor clearColor];
    //if ([[obj_list valueForKey:@"child"] count] > 0){
       cell.lblMenuName.text =[NSString stringWithFormat:@"%@",[[[[arrMenuItems objectAtIndex:indexPath.section] objectForKey:@"child"] objectAtIndex:indexPath.row] valueForKey:@"main"]];
    //}

    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 1){
        
        switch(indexPath.row){
            case 4  :
            {
                AddMatchesVC *addMatch = loadViewController(StoryBoard_InternalMatches, kIDAddMatchesVC);
                UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:addMatch];
                [self.mm_drawerController setCenterViewController:nav];
                [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
            }
                break; /* optional */
            case 0  :
            {
                MyMatchesVC *myMatches = loadViewController(StoryBoard_InternalMatches, kIDMyMatchesVC);
                UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:myMatches];
                [self.mm_drawerController setCenterViewController:nav];
                [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
            }

                break; /* optional */
            case 1 :
            {
                AllClubMatchesVC *allClucbMatches = loadViewController(StoryBoard_InternalMatches, kIDAllClubMatchesVC);
                UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:allClucbMatches];
                [self.mm_drawerController setCenterViewController:nav];
                [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
            }
                break; /* optional */
            default:{
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Info" message:[NSString stringWithFormat:@"This part of appliction is under development"] preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alertController addAction:ok];
                [self presentViewController:alertController animated:YES completion:nil];
                
            }
                break;
                
        }
       
    }else if (indexPath.section == 10){
        switch(indexPath.row){
            case 1  :
            {
                
                [self presentViewController:[[UINavigationController alloc] initWithRootViewController:vc] animated:YES completion:NULL];
            }
                
                break; /* optional */
            default:{
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Info" message:[NSString stringWithFormat:@"This part of appliction is under development"] preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alertController addAction:ok];
                [self presentViewController:alertController animated:YES completion:nil];
                
            }
                break;
                
        }

    }else{
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Info" message:[NSString stringWithFormat:@"This part of appliction is under development"] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        [self presentViewController:alertController animated:YES completion:nil];
    }

}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44.0f;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    SudeMenuTVCHeader *headerView = [tblMenu dequeueReusableCellWithIdentifier:@"idCellHeader"];
    
    if (headerView ==nil)
    {
        [tblMenu registerClass:[SudeMenuTVCHeader class] forCellReuseIdentifier:@"idCellHeader"];
        
        headerView = [tableView dequeueReusableCellWithIdentifier:@"idCellHeader"];
    }
    
    //headerView.lbTitle.text = [NSString stringWithFormat:@"Section %ld", (long)section];
    List *obj_list = [arrMenuItems objectAtIndex:section];
    headerView.lblTitle.text = [obj_list valueForKey:@"main"];
    headerView.imgiCon.image = [UIImage imageNamed:[arriCon objectAtIndex:section]];
    
    if ([[obj_list valueForKey:@"child"] count]>0){
        
        
        
        [headerView.btnExapandable setHidden:NO];
    }else{
         [headerView.btnExapandable setHidden:YES];
    }
    
    if ([arrSelectedSectionIndex containsObject:[NSNumber numberWithInteger:section]])
    {
        headerView.btnExapandable.selected = YES;
        headerView.btnTapable.selected = YES;
        
        
    }
    
    [[headerView btnExapandable] setTag:section];
    [[headerView btnTapable] setTag:section];
    
   [[headerView btnTapable] addTarget:self action:@selector(btnTapShowHideSection:) forControlEvents:UIControlEventTouchUpInside];
    //forControlEvents:UIControlEventTouchUpInside];
    
    //[headerView.contentView setBackgroundColor:section%2==0?[UIColor groupTableViewBackgroundColor]:[[UIColor groupTableViewBackgroundColor] colorWithAlphaComponent:0.5f]];
    
    return headerView.contentView;
}

-(IBAction)btnTapShowHideSection:(UIButton*)sender
{
    if (!sender.selected)
    {
        if (!isMultipleExpansionAllowed) {
            
            if([arrSelectedSectionIndex count] > 0)
            {
                [arrSelectedSectionIndex replaceObjectAtIndex:0 withObject:[NSNumber numberWithInteger:sender.tag]];
                
            }
            else{
                [arrSelectedSectionIndex addObject:[NSNumber numberWithInteger:sender.tag]];
                
            }
        }else {
            
            [arrSelectedSectionIndex addObject:[NSNumber numberWithInteger:sender.tag]];
        }
        
        sender.selected = YES;
    }else{
        sender.selected = NO;
        
        if ([arrSelectedSectionIndex containsObject:[NSNumber numberWithInteger:sender.tag]])
        {
            [arrSelectedSectionIndex removeObject:[NSNumber numberWithInteger:sender.tag]];
        }
        
        
    }
    
    if (!isMultipleExpansionAllowed) {
        [tblMenu reloadData];
    }else {
        [tblMenu reloadSections:[NSIndexSet indexSetWithIndex:sender.tag] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
    if([arrSelectedSectionIndex count] > 0)
    {
        if([sender tag] > 0){
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[arrSelectedSectionIndex count] - 1 inSection:[sender tag]];
            [tblMenu scrollToRowAtIndexPath:indexPath
                           atScrollPosition:UITableViewScrollPositionTop
                                   animated:YES];
    }
    }
    

}






@end
