//
//  SidebarVC.h
//  PingPongApp
//
//  Created by keval vasaniya on 31/07/17.
//  Copyright © 2017 keval vasaniya. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"
#import "SideMenuTVC.h"
#import "BaseVC.h"

@interface SidebarVC : BaseVC <UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tblMenu;

@property(nonatomic,strong) NSArray *items;
@property (nonatomic, retain) NSMutableArray *itemsInTable;
@property (weak, nonatomic) IBOutlet UILabel *lblUsetName;
@property (weak, nonatomic) IBOutlet UIImageView *imgUserPic;
@property (weak, nonatomic) IBOutlet UIImageView *imgClubLogo;

@end
