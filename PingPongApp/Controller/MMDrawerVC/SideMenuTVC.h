//
//  SideMenuTVC.h
//  PingPongApp
//
//  Created by keval vasaniya on 01/08/17.
//  Copyright © 2017 keval vasaniya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SideMenuTVC : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgMenu;
@property (weak, nonatomic) IBOutlet UILabel *lblMenuName;
@property (weak, nonatomic) IBOutlet UIButton *btnCxpandable;


@end
