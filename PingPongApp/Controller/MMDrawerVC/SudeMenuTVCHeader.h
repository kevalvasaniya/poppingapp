//
//  SudeMenuTVCHeader.h
//  PingPongApp
//
//  Created by keval vasaniya on 04/08/17.
//  Copyright © 2017 keval vasaniya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SudeMenuTVCHeader : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *txtTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnExapandable;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgiCon;
@property (weak, nonatomic) IBOutlet UIButton *btnTapable;

@end
