//
//  AllClubMatchesVC.m
//  PingPongApp
//
//  Created by keval vasaniya on 04/08/17.
//  Copyright © 2017 keval vasaniya. All rights reserved.
//

#import "AllClubMatchesVC.h"
#import "CalendarView.h"
#import "ClubMatchesList.h"
#import "DefaultsValues.h"
#import "DropDownMenu.h"

#define RowHeight 30.0f
@interface AllClubMatchesVC ()
{
    NSMutableArray *headData;
    NSMutableArray *leftTableData;
    NSMutableArray *rightTableData;
    CGFloat width;
    XCMultiTableView *tableView;
    UIView *myView;
    NSMutableArray *arrAllClubMatches;
    DXPopover *popover;
    int selectedButton;
    DropDownMenu *sb;    
}
@property (nonatomic, strong) CalendarView * customCalendarView;
@property (nonatomic, strong) NSCalendar * gregorian;
@property (nonatomic, assign) NSInteger currentYear;
@end

@implementation AllClubMatchesVC
@synthesize btnMatchResult,btnFromDate,btnToDate,btnDDYear,txt_search;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self navigationSetting];
   
    //Call web services
    arrAllClubMatches = [[NSMutableArray alloc]init];
    [self getAllClubMatches];
    
    // Do any additional setup after loading the view.
    width = [UIScreen mainScreen].bounds.size.width;
    [self setupLeftMenuButton];
    [self initData];
    tableView = [[XCMultiTableView alloc] initWithFrame:CGRectMake(5, btnMatchResult.frame.origin.y + btnMatchResult.frame.size.height + 30, self.view.frame.size.width - 10, (RowHeight * 3) + 50.0f)];
    //XCMultiTableView *tableView = [[XCMultiTableView alloc] initWithFrame:CGRectInset(self.view.bounds, 5.0f, 5.0f)];
    tableView.leftHeaderEnable = YES;
    tableView.datasource = self;
    tableView.delegate = self;
    [self.view addSubview:tableView];
    
    //Create view for calebnder
    [self createCalendar];
    selectedButton = 1;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initData {
    
    headData = [NSMutableArray arrayWithCapacity:12];
    [headData addObject:@"Date"];
    [headData addObject:@"Player 1"];
    [headData addObject:@"Player 2"];
    [headData addObject:@"Result"];
    [headData addObject:@"Rating"];
    [headData addObject:@"Action"];
    leftTableData = [NSMutableArray arrayWithCapacity:12];
    NSMutableArray *one = [NSMutableArray arrayWithCapacity:12];
    for (int i = 0; i < [arrAllClubMatches count]; i++) {
        ClubMatchesList *obj_matchList = [arrAllClubMatches objectAtIndex:i];
        [one addObject:[NSString stringWithFormat:@"%@", obj_matchList.internalMatchId]];
        
    }
    [leftTableData addObject:one];
    rightTableData = [NSMutableArray arrayWithCapacity:10];
    
    NSMutableArray *oneR = [NSMutableArray arrayWithCapacity:10];
    for (int i = 0; i < [arrAllClubMatches count]; i++) {
        ClubMatchesList *obj_matchList = [arrAllClubMatches objectAtIndex:i];
        NSMutableArray *ary = [NSMutableArray arrayWithCapacity:10];
        for (int j = 0; j < [headData count]; j++) {
            if (j == 0) {
                [ary addObject:obj_matchList.matchDate];
            }else
            if (j == 1) {
                [ary addObject:obj_matchList.player1];
            }else if (j == 2) {
                [ary addObject:obj_matchList.player2];
            }else if (j == 3) {
                [ary addObject:obj_matchList.matchResult];
            }else if (j == 4) {
                [ary addObject:obj_matchList.rating];
            }else if (j == 5) {
                /*UIButton *btn_view = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 100, 100)];
                [btn_view setTitle:@"Test" forState:UIControlStateNormal];
                [ary addObject:btn_view];   */
                [ary addObject:[NSString stringWithFormat:@"Remaining"]];
            }
            else {
                [ary addObject:[NSString stringWithFormat:@"column %d %d", i, j]];
            }
        }
        [oneR addObject:ary];
    }
    [rightTableData addObject:oneR];
    
    NSLog(@"Row Heighjt %f",RowHeight * [headData count]);
   
}

#pragma mark - XCMultiTableViewDataSource


- (NSArray *)arrayDataForTopHeaderInTableView:(XCMultiTableView *)tableView {
    return [headData copy];
}
- (NSArray *)arrayDataForLeftHeaderInTableView:(XCMultiTableView *)tableView InSection:(NSUInteger)section {
    return [leftTableData objectAtIndex:section];
}

- (NSArray *)arrayDataForContentInTableView:(XCMultiTableView *)tableView InSection:(NSUInteger)section {
    return [rightTableData objectAtIndex:section];
}


- (NSUInteger)numberOfSectionsInTableView:(XCMultiTableView *)tableView {
    return [leftTableData count];
}

- (AlignHorizontalPosition)tableView:(XCMultiTableView *)tableView inColumn:(NSInteger)column {
    /*if (column == 0) {
        return AlignHorizontalPositionCenter;
    }else if (column == 1) {
        return AlignHorizontalPositionRight;
    }*/
    return AlignHorizontalPositionCenter;
}

- (CGFloat)tableView:(XCMultiTableView *)tableView contentTableCellWidth:(NSUInteger)column {
    if (column == 0) {
        return width * 20 / 100;
    }else if (column == 1 || column == 2)
    {
        return width * 17 / 100;
    }else if (column == 3 || column == 4)
    {
        return width * 12 / 100;
    }
    return width * 18 / 100;
}

- (CGFloat)tableView:(XCMultiTableView *)tableView cellHeightInRow:(NSUInteger)row InSection:(NSUInteger)section {
    /*if (section == 0) {
        return 40.0f;
    }else {*/
        return RowHeight;
   // }
}

- (UIColor *)tableView:(XCMultiTableView *)tableView bgColorInSection:(NSUInteger)section InRow:(NSUInteger)row InColumn:(NSUInteger)column {
    if (row % 2 == 0 ) {
        return [self colorWithHexString:@"#F9F9F9" alpha:1];
    }
    return [self colorWithHexString:@"#E1E5E8" alpha:1];
}

- (UIColor *)tableView:(XCMultiTableView *)tableView headerBgColorInColumn:(NSUInteger)column {
    /*if (column == -1) {
        return [UIColor redColor];
    }else
    if (column == 1) {
        return [UIColor grayColor];
    }*/
//    return [UIColor clearColor];
    return [self colorWithHexString:@"#F7F7F7" alpha:1];

}

- (NSString *)vertexName {
    return @"Match ID";
}

#pragma mark - XCMultiTableViewDelegate
- (void)tableViewWithType:(MultiTableViewType)tableViewType didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"tableViewType:%@, selectedIndexPath: %@", @(tableViewType), indexPath);
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Button click event

- (IBAction)btnFromDateClick:(id)sender {
    selectedButton = 1;
    myView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width/2, 250)];
    popover = [DXPopover popover];
    [popover showAtView:btnFromDate withContentView:_customCalendarView inView:self.view];

}

- (IBAction)btnToDateClicked:(id)sender {
    selectedButton = 2;
    myView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width/2, 250)];
    popover = [DXPopover popover];
    [popover showAtView:btnToDate withContentView:_customCalendarView inView:self.view];
}

- (IBAction)btnDDYearClicked:(id)sender {
    selectedButton = 3;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:StoryBoard_InternalMatches bundle:nil];
    sb = (DropDownMenu *)[storyboard instantiateViewControllerWithIdentifier:kIDDropDownMenu];
    sb.dropDwonWidth = [NSString stringWithFormat:@"%f",btnDDYear.frame.size.width];
    sb.arrList = [[NSArray alloc]initWithObjects:@"2017", nil];;
    sb.xPossition = [NSString stringWithFormat:@"%f",btnDDYear.frame.origin.x];
    sb.yPossition = [NSString stringWithFormat:@"%f",btnDDYear.frame.origin.y + btnDDYear.frame.size.height + 1];
    [sb willMoveToParentViewController:self];
    CGRect newFrame= CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height/2);  //Change this to adjust the position of List and size of list
    [sb.view setFrame:newFrame];
    [sb.view setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:sb.view];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"selectedListItem" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(changeValue:)
                                                 name:@"selectedListItem"
                                               object:nil];
    
    
    [self addChildViewController:sb];
    [sb didMoveToParentViewController:self];
}

#pragma mark - Dropdown delegate
- (void)changeValue:(NSNotification *)notify
{
    NSString *selItem = [[notify userInfo] valueForKey:@"item"];
    NSLog(@"%@",selItem);
    if(selectedButton == 3){
        [btnDDYear setTitle:selItem forState:UIControlStateNormal];
    }
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch ;
    touch = [[event allTouches] anyObject];
    
    
    if ([touch view] == self.view) //to remove the list when tapped outside
    {
        //Do what ever you want
        [sb.view removeFromSuperview];
    }
    [sb.view removeFromSuperview];
    
}

#pragma mark - Gesture recognizer
-(void)swipeleft:(id)sender
{
    [_customCalendarView showNextMonth];
}

-(void)swiperight:(id)sender
{
    [_customCalendarView showPreviousMonth];
}

#pragma mark - CalendarDelegate protocol conformance

-(void)dayChangedToDate:(NSDate *)selectedDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"dd-MM-yyyy";
    NSTimeZone *gmt = [NSTimeZone localTimeZone];
    [dateFormatter setTimeZone:gmt];
    NSString *timeStamp = [dateFormatter stringFromDate:selectedDate];
    if(_customCalendarView.buttonPrev.isSelected || _customCalendarView.buttonNext.isSelected){
        //Do nothing
    }
    else{
        if (selectedButton == 1) {
            [btnFromDate setTitle:timeStamp forState:UIControlStateNormal];
        }else{
            [btnToDate setTitle:timeStamp forState:UIControlStateNormal];
        }
        
        [popover dismiss];
    }
}

#pragma mark - Create Calendar
-(void)createCalendar{
    [self.view setBackgroundColor:[UIColor whiteColor]];
    self.title = @"All Assignment";
    _gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    _customCalendarView = [[CalendarView alloc]initWithFrame:CGRectMake(0, 0, 200,( self.view.frame.size.height /3.2) )];
    _customCalendarView.delegate = (id)self;
    _customCalendarView.datasource = (id)self;
    _customCalendarView.calendarDate = [NSDate date];
    _customCalendarView.monthAndDayTextColor = [UIColor whiteColor];
    _customCalendarView.dayTextColor = [UIColor grayColor];
    _customCalendarView.borderColor = [UIColor blackColor];
    _customCalendarView.dayBgColorWithData = [UIColor lightGrayColor];
    _customCalendarView.dayBgColorWithoutData = [UIColor clearColor];
    _customCalendarView.calenderWidth = 200;
    _customCalendarView.endScreenWidht = self.view.frame.size.width - 29;
    _customCalendarView.dayTxtColorWithoutData = [UIColor grayColor];
    _customCalendarView.dayTxtColorWithData = [UIColor whiteColor];
    _customCalendarView.dayTxtColorSelected = [UIColor whiteColor];
    _customCalendarView.borderColor = RGBCOLOR(159, 162, 172);
    _customCalendarView.borderWidth  = 0;
    _customCalendarView.allowsChangeMonthByDayTap = YES;
    _customCalendarView.allowsChangeMonthByButtons = YES;
    _customCalendarView.keepSelDayWhenMonthChange = YES;
    _customCalendarView.nextMonthAnimation = UIViewAnimationOptionTransitionCurlUp;
    _customCalendarView.prevMonthAnimation = UIViewAnimationOptionTransitionCurlDown;
    _customCalendarView.backgroundColor = [UIColor whiteColor];
    NSDateComponents * yearComponent = [_gregorian components:NSCalendarUnitYear fromDate:[NSDate date]];
    _currentYear = yearComponent.year;
    [myView addSubview:_customCalendarView];
}

#pragma mark - CalendarDataSource protocol conformance

-(BOOL)isDataForDate:(NSDate *)date
{
    /*if ([date compare:[NSDate date]] == NSOrderedAscending)
     return YES;
     return NO;*/
    NSDateFormatter *dateformatter = [[NSDateFormatter alloc]init];
    dateformatter.dateFormat = @"yyyy-MM-dd";
    //NSString *dateCompare = [dateformatter stringFromDate:date];
    
    return YES;
}

-(BOOL)canSwipeToDate:(NSDate *)date
{
    NSDateComponents * yearComponent = [_gregorian components:NSCalendarUnitYear fromDate:date];
    /*.tblView.frame = CGRectMake(self.tblView.frame.origin.x, self.calenderView.frame.origin.y + self.calenderView.frame.size.height + 40, self.tblView.frame.size.width, self.tblView.frame.size.height);
     [self.tblView reloadData];*/
    return (yearComponent.year == _currentYear || yearComponent.year == _currentYear+1);
}

#pragma mark - Hext to color
- (UIColor *)colorWithHexString:(NSString *)str_HEX  alpha:(CGFloat)alpha_range{
    int red = 0;
    int green = 0;
    int blue = 0;
    sscanf([str_HEX UTF8String], "#%02X%02X%02X", &red, &green, &blue);
    return  [UIColor colorWithRed:red/255.0 green:green/255.0 blue:blue/255.0 alpha:alpha_range];
}

#pragma mark - Web servies calling for get the list of All club matches
- (void)getAllClubMatches
{
    [SVProgressHUD showWithStatus:@"Loading Details"];
   
    [[WebServiceConnector alloc]init:[NSString stringWithFormat:@"%@%@=%@&%@=%@&%@=%@",URL_AllClubMatches,ClubId,@"8",Page,@"1",Limit,@"1000"]
                      withParameters:@{}
                          withObject:self
                        withSelector:@selector(displayAllCLubMatchesResponse:)
                      forServiceType:@"POST"
                      showDisplayMsg:AllClubMatchesMsg
                showNetworkIndicator:NO];
}

- (IBAction)displayAllCLubMatchesResponse:(id)sender
{
    arrAllClubMatches = (NSMutableArray *)[sender responseArray];
    NSLog(@"%lu Array is", (unsigned long)[arrAllClubMatches count]);
    //[tableView reloadData];
    [self initData];
    [tableView removeFromSuperview];
    if([arrAllClubMatches count] > 8){
        tableView = [[XCMultiTableView alloc] initWithFrame:CGRectMake(5, txt_search.frame.origin.y + txt_search.frame.size.height + 20, self.view.frame.size.width - 10, (RowHeight * 8) + 50.0f)];

    }else{
    tableView = [[XCMultiTableView alloc] initWithFrame:CGRectMake(5, txt_search.frame.origin.y + txt_search.frame.size.height + 20, self.view.frame.size.width - 10, (RowHeight * [arrAllClubMatches count]) + 50.0f)];
    }
    
    tableView.leftHeaderEnable = YES;
    tableView.datasource = self;
    tableView.delegate = self;
    [self.view addSubview:tableView];
}

- (IBAction)tempClick:(id)sender {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Alert" message:@"This part is under development because of need to develop API for filter data." preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    [self presentViewController:alertController animated:YES completion:nil];

}


@end
