//
//  AllClubMatchesVC.h
//  PingPongApp
//
//  Created by keval vasaniya on 04/08/17.
//  Copyright © 2017 keval vasaniya. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseVC.h"
#import "XCMultiSortTableView.h"
#import "DXPopover.h"
#import "CalendarView.h"

@interface AllClubMatchesVC : BaseVC <XCMultiTableViewDataSource, XCMultiTableViewDelegate>
- (IBAction)btnFromDateClick:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnFromDate;
@property (weak, nonatomic) IBOutlet UIView *inView;
@property (weak, nonatomic) IBOutlet UIView *viewTemp;
@property (weak, nonatomic) IBOutlet UIButton *btnToDate;
@property (weak, nonatomic) IBOutlet UIButton *btnDDYear;
@property (weak, nonatomic) IBOutlet UITextField *txt_search;

@property (weak, nonatomic) IBOutlet UIButton *btnMatchResult;
- (IBAction)tempClick:(id)sender;
- (IBAction)btnToDateClicked:(id)sender;
- (IBAction)btnDDYearClicked:(id)sender;

@end
