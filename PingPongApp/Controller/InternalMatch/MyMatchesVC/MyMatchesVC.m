//
//  MyMatchesVC.m
//  PingPongApp
//
//  Created by keval vasaniya on 04/08/17.
//  Copyright © 2017 keval vasaniya. All rights reserved.
//

#import "MyMatchesVC.h"
#import "DefaultsValues.h"
#import "ClubMatchesList.h"
#import "XCMultiSortTableView.h"
#import "CalendarView.h"
#import "DXPopover.h"
#import "DropDownMenu.h"

#define RowHeight 30.0f

@interface MyMatchesVC ()
{
    NSMutableArray *arrMyMatch;
    NSMutableArray *headData;
    NSMutableArray *leftTableData;
    NSMutableArray *rightTableData;
    XCMultiTableView *tableView;
    CGFloat width;
    int selectedButton;
    UIView *myView;
    DXPopover *popover;
    DropDownMenu *sb;
}
@property (nonatomic, strong) CalendarView * customCalendarView;
@property (nonatomic, strong) NSCalendar * gregorian;
@property (nonatomic, assign) NSInteger currentYear;
@end

@implementation MyMatchesVC
@synthesize btnMatchResult,btnFromDate, btnToDate, btnDDYear;
- (void)viewDidLoad {
    [super viewDidLoad];
    //Set the color of navigation bar
    [self navigationSetting];
    // Do any additional setup after loading the view.
    [self setupLeftMenuButton];
    arrMyMatch = [[NSMutableArray alloc]init];
    [self getMyClubMatches];
    width = [UIScreen mainScreen].bounds.size.width;
    selectedButton = 1;
    //Create view for calebnder
    [self createCalendar];
    /*tableView = [[XCMultiTableView alloc] initWithFrame:CGRectMake(5, 80, self.view.frame.size.width - 10, (RowHeight * 3) + 50.0f)];
    //XCMultiTableView *tableView = [[XCMultiTableView alloc] initWithFrame:CGRectInset(self.view.bounds, 5.0f, 5.0f)];
    tableView.leftHeaderEnable = YES;
    tableView.datasource = (id)self;
    tableView.delegate = (id)self;
    [self.view addSubview:tableView];*/
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)initData {
    
    headData = [NSMutableArray arrayWithCapacity:12];
    [headData addObject:@"Date"];
    [headData addObject:@"Player 1"];
    [headData addObject:@"Player 2"];
    [headData addObject:@"Result"];
    [headData addObject:@"Rating"];
    [headData addObject:@"Action"];
    leftTableData = [NSMutableArray arrayWithCapacity:12];
    NSMutableArray *one = [NSMutableArray arrayWithCapacity:12];
    for (int i = 0; i < [arrMyMatch count]; i++) {
        ClubMatchesList *obj_matchList = [arrMyMatch objectAtIndex:i];
        [one addObject:[NSString stringWithFormat:@"%@", obj_matchList.internalMatchId]];
        
    }
    [leftTableData addObject:one];
    /*NSMutableArray *two = [NSMutableArray arrayWithCapacity:12];
     for (int i = 3; i < 10; i++) {
     [two addObject:[NSString stringWithFormat:@"ki-%d", i]];
     }
     [leftTableData addObject:two];*/
    
    rightTableData = [NSMutableArray arrayWithCapacity:10];
    
    NSMutableArray *oneR = [NSMutableArray arrayWithCapacity:10];
    for (int i = 0; i < [arrMyMatch count]; i++) {
        ClubMatchesList *obj_matchList = [arrMyMatch objectAtIndex:i];
        NSMutableArray *ary = [NSMutableArray arrayWithCapacity:10];
        for (int j = 0; j < [headData count]; j++) {
            if (j == 0) {
                [ary addObject:obj_matchList.matchDate];
            }else
                if (j == 1) {
                    [ary addObject:obj_matchList.player1];
                }else if (j == 2) {
                    [ary addObject:obj_matchList.player2];
                }else if (j == 3) {
                    [ary addObject:obj_matchList.matchResult];
                }else if (j == 4) {
                    [ary addObject:obj_matchList.rating];
                }else if (j == 5) {
                    /*UIButton *btn_view = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 100, 100)];
                     [btn_view setTitle:@"Test" forState:UIControlStateNormal];
                     [ary addObject:btn_view];   */
                    [ary addObject:[NSString stringWithFormat:@"Remaining"]];
                }
                else {
                    [ary addObject:[NSString stringWithFormat:@"column %d %d", i, j]];
                }
        }
        [oneR addObject:ary];
    }
    [rightTableData addObject:oneR];
    
    NSLog(@"Row Heighjt %f",RowHeight * [headData count]);
    
    /*NSMutableArray *twoR = [NSMutableArray arrayWithCapacity:10];
     for (int i = 3; i < 10; i++) {
     NSMutableArray *ary = [NSMutableArray arrayWithCapacity:10];
     for (int j = 0; j < [headData count]; j++) {
     if (j == 1) {
     [ary addObject:[NSNumber numberWithInt:random() % 5]];
     }else if (j == 2) {
     [ary addObject:[NSNumber numberWithInt:random() % 5]];
     }else {
     [ary addObject:[NSString stringWithFormat:@"column %d %d", i, j]];
     }
     }
     [twoR addObject:ary];
     }
     [rightTableData addObject:twoR];*/
}

#pragma mark - XCMultiTableViewDataSource


- (NSArray *)arrayDataForTopHeaderInTableView:(XCMultiTableView *)tableView {
    return [headData copy];
}
- (NSArray *)arrayDataForLeftHeaderInTableView:(XCMultiTableView *)tableView InSection:(NSUInteger)section {
    return [leftTableData objectAtIndex:section];
}

- (NSArray *)arrayDataForContentInTableView:(XCMultiTableView *)tableView InSection:(NSUInteger)section {
    return [rightTableData objectAtIndex:section];
}


- (NSUInteger)numberOfSectionsInTableView:(XCMultiTableView *)tableView {
    return [leftTableData count];
}

- (AlignHorizontalPosition)tableView:(XCMultiTableView *)tableView inColumn:(NSInteger)column {
    /*if (column == 0) {
     return AlignHorizontalPositionCenter;
     }else if (column == 1) {
     return AlignHorizontalPositionRight;
     }*/
    return AlignHorizontalPositionCenter;
}

- (CGFloat)tableView:(XCMultiTableView *)tableView contentTableCellWidth:(NSUInteger)column {
    if (column == 0) {
        return width * 20 / 100;
    }else if (column == 1 || column == 2)
    {
        return width * 17 / 100;
    }else if (column == 3 || column == 4)
    {
        return width * 12 / 100;
    }
    return width * 18 / 100;
}

- (CGFloat)tableView:(XCMultiTableView *)tableView cellHeightInRow:(NSUInteger)row InSection:(NSUInteger)section {
    /*if (section == 0) {
     return 40.0f;
     }else {*/
    return RowHeight;
    // }
}

- (UIColor *)tableView:(XCMultiTableView *)tableView bgColorInSection:(NSUInteger)section InRow:(NSUInteger)row InColumn:(NSUInteger)column {
    if (row % 2 == 0 ) {
        return [self colorWithHexString:@"#F9F9F9" alpha:1];
    }
    return [self colorWithHexString:@"#E1E5E8" alpha:1];
}

- (UIColor *)tableView:(XCMultiTableView *)tableView headerBgColorInColumn:(NSUInteger)column {
    /*if (column == -1) {
     return [UIColor redColor];
     }else
     if (column == 1) {
     return [UIColor grayColor];
     }*/
    //    return [UIColor clearColor];
    return [self colorWithHexString:@"#F7F7F7" alpha:1];
    
}

- (NSString *)vertexName {
    return @"Match ID";
}

#pragma mark - XCMultiTableViewDelegate
- (void)tableViewWithType:(MultiTableViewType)tableViewType didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"tableViewType:%@, selectedIndexPath: %@", @(tableViewType), indexPath);
}



#pragma mark - Web servies calling for get the list of my club matches
- (void)getMyClubMatches
{
    [SVProgressHUD showWithStatus:@"Loading Details"];
    NSLog(@"%@",[NSString stringWithFormat:@"%@%@=%@",URL_MyMatchList,MemberId,[DefaultsValues getStringValueFromUserDefaults_ForKey:kUserID]]);
    [[WebServiceConnector alloc]init:[NSString stringWithFormat:@"%@%@=%@",URL_MyMatchList,MemberId,@"39"]
                      withParameters:@{}
                          withObject:self
                        withSelector:@selector(displayMyMatchesResponse:)
                      forServiceType:@"POST"
                      showDisplayMsg:AllMyMatchesMsg
                showNetworkIndicator:NO];
}

- (IBAction)displayMyMatchesResponse:(id)sender
{
    arrMyMatch = (NSMutableArray *)[sender responseArray];
    //[tableView reloadData];
    if([arrMyMatch count] > 0){
    [self initData];
    [tableView removeFromSuperview];
    
    if([arrMyMatch count] > 8){
        tableView = [[XCMultiTableView alloc] initWithFrame:CGRectMake(5,btnMatchResult.frame.origin.y + btnMatchResult.frame.size.height + 30, self.view.frame.size.width - 10, (RowHeight * 8) + 50.0f)];
        
    }else{
        tableView = [[XCMultiTableView alloc] initWithFrame:CGRectMake(5,btnMatchResult.frame.origin.y + btnMatchResult.frame.size.height + 30, self.view.frame.size.width - 10, (RowHeight * [arrMyMatch count]) + 50.0f)];
    }
    
    tableView.leftHeaderEnable = YES;
    tableView.datasource = (id)self;
    tableView.delegate = (id)self;
    [self.view addSubview:tableView];
    }
}

#pragma mark - button click event

- (IBAction)btnFromDateClick:(id)sender {
    selectedButton = 1;
    myView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width/2, 250)];
    popover = [DXPopover popover];
    [popover showAtView:btnFromDate withContentView:_customCalendarView inView:self.view];
}

- (IBAction)btnToDateClick:(id)sender {
    selectedButton = 2;
    myView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width/2, 250)];
    popover = [DXPopover popover];
    [popover showAtView:btnToDate withContentView:_customCalendarView inView:self.view];
}

- (IBAction)btnDDYearClick:(id)sender {
    selectedButton = 3;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:StoryBoard_InternalMatches bundle:nil];
    sb = (DropDownMenu *)[storyboard instantiateViewControllerWithIdentifier:kIDDropDownMenu];
    sb.dropDwonWidth = [NSString stringWithFormat:@"%f",btnDDYear.frame.size.width];
    sb.arrList = [[NSArray alloc]initWithObjects:@"2017", nil];;
    sb.xPossition = [NSString stringWithFormat:@"%f",btnDDYear.frame.origin.x];
    sb.yPossition = [NSString stringWithFormat:@"%f",btnDDYear.frame.origin.y + btnDDYear.frame.size.height + 1];
    [sb willMoveToParentViewController:self];
    CGRect newFrame= CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height/2);  //Change this to adjust the position of List and size of list
    [sb.view setFrame:newFrame];
    [sb.view setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:sb.view];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"selectedListItem" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(changeValue:)
                                                 name:@"selectedListItem"
                                               object:nil];
    
    
    [self addChildViewController:sb];
    [sb didMoveToParentViewController:self];

}

- (IBAction)btnMatchResultClick:(id)sender {
    selectedButton = 4;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:StoryBoard_InternalMatches bundle:nil];
    sb = (DropDownMenu *)[storyboard instantiateViewControllerWithIdentifier:kIDDropDownMenu];
    sb.dropDwonWidth = [NSString stringWithFormat:@"%f",btnMatchResult.frame.size.width];
    sb.arrList = [[NSArray alloc]initWithObjects:@"Display won matches",@"Display lost matches", nil];;
    sb.xPossition = [NSString stringWithFormat:@"%f",btnMatchResult.frame.origin.x];
    sb.yPossition = [NSString stringWithFormat:@"%f",btnMatchResult.frame.origin.y + btnMatchResult.frame.size.height + 1];
    [sb willMoveToParentViewController:self];
    CGRect newFrame= CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height/2);  //Change this to adjust the position of List and size of list
    [sb.view setFrame:newFrame];
    [sb.view setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:sb.view];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"selectedListItem" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(changeValue:)
                                                 name:@"selectedListItem"
                                               object:nil];
    
    
    [self addChildViewController:sb];
    [sb didMoveToParentViewController:self];
}

#pragma mark - Gesture recognizer
-(void)swipeleft:(id)sender
{
    [_customCalendarView showNextMonth];
}

-(void)swiperight:(id)sender
{
    [_customCalendarView showPreviousMonth];
}

#pragma mark - CalendarDelegate protocol conformance

-(void)dayChangedToDate:(NSDate *)selectedDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"dd-MM-yyyy";
    NSTimeZone *gmt = [NSTimeZone localTimeZone];
    [dateFormatter setTimeZone:gmt];
    NSString *timeStamp = [dateFormatter stringFromDate:selectedDate];
    if(_customCalendarView.buttonPrev.isSelected || _customCalendarView.buttonNext.isSelected){
        //Do nothing
    }
    else{
        if (selectedButton == 1) {
            [btnFromDate setTitle:timeStamp forState:UIControlStateNormal];
        }else{
            [btnToDate setTitle:timeStamp forState:UIControlStateNormal];
        }
        
        [popover dismiss];
    }
}

//hh/h/h/h/h/h/h/htytrytryrtytryrtyefersfgersfgsfgs

#pragma mark - Create Calendar
-(void)createCalendar{
    [self.view setBackgroundColor:[UIColor whiteColor]];
    self.title = @"All Assignment";
    _gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    _customCalendarView = [[CalendarView alloc]initWithFrame:CGRectMake(0, 0, 200,( self.view.frame.size.height /3.2) )];
    _customCalendarView.delegate = (id)self;
    _customCalendarView.datasource = (id)self;
    _customCalendarView.calendarDate = [NSDate date];
    _customCalendarView.monthAndDayTextColor = [UIColor whiteColor];
    _customCalendarView.dayTextColor = [UIColor grayColor];
    _customCalendarView.borderColor = [UIColor blackColor];
    _customCalendarView.dayBgColorWithData = [UIColor lightGrayColor];
    _customCalendarView.dayBgColorWithoutData = [UIColor clearColor];
    _customCalendarView.calenderWidth = 200;
    _customCalendarView.endScreenWidht = self.view.frame.size.width - 29;
    _customCalendarView.dayTxtColorWithoutData = [UIColor grayColor];
    _customCalendarView.dayTxtColorWithData = [UIColor whiteColor];
    _customCalendarView.dayTxtColorSelected = [UIColor whiteColor];
    _customCalendarView.borderColor = RGBCOLOR(159, 162, 172);
    _customCalendarView.borderWidth  = 0;
    _customCalendarView.allowsChangeMonthByDayTap = YES;
    _customCalendarView.allowsChangeMonthByButtons = YES;
    _customCalendarView.keepSelDayWhenMonthChange = YES;
    _customCalendarView.nextMonthAnimation = UIViewAnimationOptionTransitionCurlUp;
    _customCalendarView.prevMonthAnimation = UIViewAnimationOptionTransitionCurlDown;
    _customCalendarView.backgroundColor = [UIColor whiteColor];
    NSDateComponents * yearComponent = [_gregorian components:NSCalendarUnitYear fromDate:[NSDate date]];
    _currentYear = yearComponent.year;
    [myView addSubview:_customCalendarView];
}

#pragma mark - CalendarDataSource protocol conformance

-(BOOL)isDataForDate:(NSDate *)date
{
    /*if ([date compare:[NSDate date]] == NSOrderedAscending)
     return YES;
     return NO;*/
    NSDateFormatter *dateformatter = [[NSDateFormatter alloc]init];
    dateformatter.dateFormat = @"yyyy-MM-dd";
    //NSString *dateCompare = [dateformatter stringFromDate:date];
    
    return YES;
}

-(BOOL)canSwipeToDate:(NSDate *)date
{
    NSDateComponents * yearComponent = [_gregorian components:NSCalendarUnitYear fromDate:date];
    /*.tblView.frame = CGRectMake(self.tblView.frame.origin.x, self.calenderView.frame.origin.y + self.calenderView.frame.size.height + 40, self.tblView.frame.size.width, self.tblView.frame.size.height);
     [self.tblView reloadData];*/
    return (yearComponent.year == _currentYear || yearComponent.year == _currentYear+1);
}

#pragma mark - Dropdown delegate
- (void)changeValue:(NSNotification *)notify
{
    NSString *selItem = [[notify userInfo] valueForKey:@"item"];
    NSLog(@"%@",selItem);
    if(selectedButton == 3){
        [btnDDYear setTitle:selItem forState:UIControlStateNormal];
    }else if(selectedButton == 4){
        [btnMatchResult setTitle:selItem forState:UIControlStateNormal];
    }
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch ;
    touch = [[event allTouches] anyObject];
    
    
    if ([touch view] == self.view) //to remove the list when tapped outside
    {
        //Do what ever you want
        [sb.view removeFromSuperview];
    }
    [sb.view removeFromSuperview];
    
}






@end
