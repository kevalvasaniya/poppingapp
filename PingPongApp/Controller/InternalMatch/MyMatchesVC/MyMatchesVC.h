//
//  MyMatchesVC.h
//  PingPongApp
//
//  Created by keval vasaniya on 04/08/17.
//  Copyright © 2017 keval vasaniya. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseVC.h"

@interface MyMatchesVC : BaseVC
@property (weak, nonatomic) IBOutlet UIButton *btnMatchResult;
@property (weak, nonatomic) IBOutlet UIButton *btnFromDate;
@property (weak, nonatomic) IBOutlet UIButton *btnToDate;
@property (weak, nonatomic) IBOutlet UIButton *btnDDYear;

- (IBAction)btnFromDateClick:(id)sender;

- (IBAction)btnToDateClick:(id)sender;
- (IBAction)btnDDYearClick:(id)sender;
- (IBAction)btnMatchResultClick:(id)sender;

@end
