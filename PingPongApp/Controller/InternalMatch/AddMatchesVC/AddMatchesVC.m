//
//  AddMatchesVC.m
//  PingPongApp
//
//  Created by keval vasaniya on 04/08/17.
//  Copyright © 2017 keval vasaniya. All rights reserved.
//

#import "AddMatchesVC.h"
#import "HomeVC.h"
#import "DXPopover.h"
#import "DefaultsValues.h"
#import "MemberList.h"
#import "DropDownMenu.h"

@interface AddMatchesVC ()
{
    UIView *myView;
    UIButton *btn_Calender;
    DXPopover *popover ;
    UITextField *txtCurrentTextfiled;
    NSString *localGametype;
    NSMutableArray *data;
    NSMutableArray *arrMeberList;
    DropDownMenu *sb;;
    int player1Score ;
    int player2Score ;
    BOOL isClickOnAC;
    int member2Id;
    NSMutableDictionary *dictMemberRating;
}
@property (nonatomic, strong) CalendarView * customCalendarView;
@property (nonatomic, strong) NSCalendar * gregorian;
@property (nonatomic, assign) NSInteger currentYear;
@end

@implementation AddMatchesVC

- (void)viewDidLoad {
    [super viewDidLoad];
    player1Score = 0;
    player2Score = 0;
    isClickOnAC = false;
    member2Id = 0;
    //Set the color of navigation bar
    [self navigationSetting];
    //[ self.navigationController.navigationBar setBarTintColor :[self colorWithHexString:@"#2A3F54" alpha:1] ];
   self.navigationController.navigationBar.barTintColor = [self colorWithHexString:@"#2A3F54" alpha:1];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
       // Do any additional setup after loading the view.
     [self setupLeftMenuButton];
    
    _viewGame1.hidden = NO;
    _viewGame3.hidden = YES;
    _viewGame5.hidden = YES;
    _viewGame7.hidden = YES;
    
    _viewGame1.alpha = 1;
    _viewGame3.alpha = 0;
    _viewGame5.alpha = 0;
    _viewGame7.alpha = 0;
    
    localGametype = @"1";
    
    _viewButton.frame = CGRectMake(_viewButton.frame.origin.x,  _viewGame1.frame.origin.y + _viewGame1.frame.size.height  , _viewButton.frame.size.width, _viewButton.frame.size.height);
    
    _scrollView.contentSize = CGSizeMake(0, _viewButton.frame.origin.y + _viewButton.frame.size.height );
    
    //Set border of textfiled
    [self setTextFeildBorder:_txtPlayer1 borderColor:@"#ECEEEE"];
    [self setTextFeildBorder:_txtPlayer1Rating borderColor:@"#ECEEEE"];
    [self setTextFeildBorder:_txtPlayer2 borderColor:@"#ECEEEE"];
    [self setTextFeildBorder:_txtPlayer2Rating borderColor:@"#ECEEEE"];
    [self setTextFeildBorder:_txtMatchDate borderColor:@"#ECEEEE"];
    [self setTextFeildBorder:_tatMatchDay borderColor:@"#ECEEEE"];
    [self setTextFeildBorder:_txtGame1 borderColor:@"#ECEEEE"];
    [self setTextFeildBorder:_txtGame2 borderColor:@"#ECEEEE"];
    [self setTextFeildBorder:_txtGame3 borderColor:@"#ECEEEE"];
    [self setTextFeildBorder:_txtGame4 borderColor:@"#ECEEEE"];
    [self setTextFeildBorder:_txtGame5 borderColor:@"#ECEEEE"];
    [self setTextFeildBorder:_txtGame6 borderColor:@"#ECEEEE"];
    [self setTextFeildBorder:_txtGame7 borderColor:@"#ECEEEE"];
    [self setTextFeildBorder:_txtMatchScore borderColor:@"#ECEEEE"];
    [self setTextFeildBorder:_txtMatchResult borderColor:@"#ECEEEE"];
    
    //Set border of Textview
    [self setTextViewBorder:_txtPlayer1Comment borderColor:@"#ECEEEE"];
    [self setTextViewBorder:_txtPlayer2Comment borderColor:@"#ECEEEE"];
    [self setTextViewBorder:_view2txtPlayer1Comment borderColor:@"#ECEEEE"];
    [self setTextViewBorder:_view2txtPlayer2Comment borderColor:@"#ECEEEE"];
    [self setTextViewBorder:_view3txtPlayer1Comment borderColor:@"#ECEEEE"];
    [self setTextViewBorder:_view3txtPlayer2Comment borderColor:@"#ECEEEE"];
    [self setTextViewBorder:_view4txtPlayer1Comment borderColor:@"#ECEEEE"];
    [self setTextViewBorder:_view4txtPlayer2Comment borderColor:@"#ECEEEE"];
    
    [_txtGame1 addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    _txtGame1.clearButtonMode = UITextFieldViewModeWhileEditing;
    _txtGame1.delegate = (id)self;
    [_txtGame2 addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    _txtGame2.clearButtonMode = UITextFieldViewModeWhileEditing;
    _txtGame2.delegate = (id)self;
    [_txtGame3 addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    _txtGame3.clearButtonMode = UITextFieldViewModeWhileEditing;
    _txtGame3.delegate = (id)self;
    [_txtGame4 addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    _txtGame4.clearButtonMode = UITextFieldViewModeWhileEditing;
    _txtGame4.delegate = (id)self;
    [_txtGame5 addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    _txtGame5.clearButtonMode = UITextFieldViewModeWhileEditing;
    _txtGame5.delegate = (id)self;
    [_txtGame6 addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    _txtGame6.clearButtonMode = UITextFieldViewModeWhileEditing;
    _txtGame6.delegate = (id)self;
    [_txtGame7 addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    _txtGame7.clearButtonMode = UITextFieldViewModeWhileEditing;
    _txtGame7.delegate = (id)self;

    
    [_v3txtGame1 addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    _v3txtGame1.clearButtonMode = UITextFieldViewModeWhileEditing;
    _v3txtGame1.delegate = (id)self;
    [_v3txtGame2 addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    _v3txtGame2.clearButtonMode = UITextFieldViewModeWhileEditing;
    _v3txtGame2.delegate = (id)self;
    [_v3txtGame3 addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    _v3txtGame3.clearButtonMode = UITextFieldViewModeWhileEditing;
    _v3txtGame3.delegate = (id)self;
    [_v3txtGame4 addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    _v3txtGame4.clearButtonMode = UITextFieldViewModeWhileEditing;
    _v3txtGame4.delegate = (id)self;
    [_v3txtGame5 addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    _v3txtGame5.clearButtonMode = UITextFieldViewModeWhileEditing;
    _v3txtGame5.delegate = (id)self;
    
    [_v2txtGame1 addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    _v2txtGame1.clearButtonMode = UITextFieldViewModeWhileEditing;
    _v2txtGame1.delegate = (id)self;
    [_v2txtGame2 addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    _v2txtGame2.clearButtonMode = UITextFieldViewModeWhileEditing;
    _v2txtGame2.delegate = (id)self;
    [_v2txtGame3 addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    _v2txtGame3.clearButtonMode = UITextFieldViewModeWhileEditing;
    _v2txtGame3.delegate = (id)self;

    [_v1txtGame1 addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    _v1txtGame1.clearButtonMode = UITextFieldViewModeWhileEditing;
    _v1txtGame1.delegate = (id)self;
    
    [_txtMatchResult addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    _txtMatchResult.clearButtonMode = UITextFieldViewModeWhileEditing;
    _txtMatchResult.delegate = (id)self;
    [_txtMatchScore addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    _txtMatchScore.clearButtonMode = UITextFieldViewModeWhileEditing;
    _txtMatchScore.delegate = (id)self;
    
    [_v1txtMatchResult addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    _v1txtMatchResult.clearButtonMode = UITextFieldViewModeWhileEditing;
    _v1txtMatchResult.delegate = (id)self;
    [_v1txtMatchScore addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    _v1txtMatchScore.clearButtonMode = UITextFieldViewModeWhileEditing;
    _v1txtMatchScore.delegate = (id)self;
    
    [_v2txtMatchResult addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    _v2txtMatchResult.clearButtonMode = UITextFieldViewModeWhileEditing;
    _v2txtMatchResult.delegate = (id)self;
    [_v2txtMatchScore addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    _v2txtMatchScore.clearButtonMode = UITextFieldViewModeWhileEditing;
    _v2txtMatchScore.delegate = (id)self;
    
    [_v3txtMatchResult addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    _v3txtMatchResult.clearButtonMode = UITextFieldViewModeWhileEditing;
    _v3txtMatchResult.delegate = (id)self;
    [_v3txtMatchScore addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    _v3txtMatchScore.clearButtonMode = UITextFieldViewModeWhileEditing;
    _v3txtMatchScore.delegate = (id)self;

    [_txtPlayer2 addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    _txtPlayer2.clearButtonMode = UITextFieldViewModeWhileEditing;
    _txtPlayer2.delegate = (id)self;
    
    btn_Calender = [UIButton buttonWithType:UIButtonTypeCustom];
    btn_Calender.frame = CGRectMake(0, _txtMatchDate.frame.origin.y, _txtMatchDate.frame.size.height, _txtMatchDate.frame.size.height);
    [btn_Calender setImage:[UIImage imageNamed:@"calender_icon"] forState:UIControlStateNormal];
    _txtMatchDate.rightView = btn_Calender;
    _txtMatchDate.rightViewMode = UITextFieldViewModeUnlessEditing;
    
    [btn_Calender addTarget:self action:@selector(btnCalenderClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [self createCalendar];
    
    _txtPlayer1.text = [DefaultsValues getStringValueFromUserDefaults_ForKey:kFirstName];
    
    
    
}

-(void)textFieldDidChange :(UITextField *)theTextField{
    txtCurrentTextfiled = theTextField;
    if(theTextField == _txtMatchScore || theTextField == _txtMatchResult || theTextField == _v3txtMatchScore || theTextField == _v3txtMatchResult || theTextField == _v2txtMatchScore || theTextField == _v2txtMatchResult || theTextField == _v1txtMatchScore || theTextField == _v1txtMatchResult){
        
        if([theTextField.text length] == 1){
            theTextField.text = [NSString stringWithFormat:@"%@-",theTextField.text];
            
        }
        if([theTextField.text length] == 3){
             [self checkScoreCorrectnessWS:localGametype MatchScore:theTextField.text];
            
        }
        else if([theTextField.text length] == 4){
            NSString *lastCharacter =[theTextField.text substringWithRange:NSMakeRange(3, 1)];
            theTextField.text = [NSString stringWithFormat:@"%@-",lastCharacter];
        }
        
    }else if(theTextField == _txtPlayer2){
        if([_txtPlayer2.text length] == 0)
        {
            [sb.view removeFromSuperview];
            arrMeberList = [[NSMutableArray alloc]init];
//            [self MemberListWS:_txtPlayer2.text];
        }else{
            isClickOnAC = false;
            [self MemberListWS:_txtPlayer2.text];
        }
        
    }
    else{
        if([theTextField.text length] == 2){
            theTextField.text = [NSString stringWithFormat:@"%@-",theTextField.text];
            
        }
        else if([theTextField.text length] == 5){
            [self checkScoreCorrectnessWS:localGametype MatchScore:theTextField.text];
        }
        else if([theTextField.text length] == 6){
            NSString *lastCharacter =[theTextField.text substringWithRange:NSMakeRange(5, 1)];
            theTextField.text = lastCharacter;
        }
    }
    
}

-(BOOL)textFieldShouldClear:(UITextField *)textField
{
    
    [textField resignFirstResponder];
    if(_v1txtGame1 == textField){
        _v1txtMatchResult.text = @"";
        _v1txtMatchScore.text = @"";
    }
    if(_v2txtGame1 == textField || _v2txtGame2 == textField || _v2txtGame3 == textField){
        _v2txtGame3.enabled = true;
        [self setMatchResult:_v2txtMatchResult MatchScore:@""];
        if([[self splitString:textField.text splitBy:@"-"][0] intValue] > [[self splitString:textField.text splitBy:@"-"][1] intValue]){
            if(player1Score > 0)
                player1Score = player1Score - 1;
        }else{
            if(player2Score > 0)
                player2Score = player2Score - 1;
        }
        _v2txtMatchScore.text = [NSString stringWithFormat:@"%d-%d",player1Score,player2Score];
    }
    if(_v3txtGame1 == textField || _v3txtGame2 == textField || _v3txtGame3 == textField || _v3txtGame4 == textField || _v3txtGame5 == textField){
        _txtGame5.enabled = true;
        _txtGame6.enabled = true;
        _txtGame7.enabled = true;
        [self setMatchResult:_txtMatchResult MatchScore:@""];
        if([[self splitString:textField.text splitBy:@"-"][0] intValue] > [[self splitString:textField.text splitBy:@"-"][1] intValue]){
            if(player1Score > 0)
                player1Score = player1Score - 1;
        }else{
            if(player2Score > 0)
                player2Score = player2Score - 1;
        }
        _txtMatchScore.text = [NSString stringWithFormat:@"%d-%d",player1Score,player2Score];
    }
    if(_txtGame1 == textField || _txtGame2 == textField || _txtGame3 == textField || _txtGame4 == textField || _txtGame5 == textField || _txtGame6 == textField || _txtGame6 == textField){
        _v3txtGame4.enabled = true;
        _v3txtGame5.enabled = true;
        [self setMatchResult:_v3txtMatchResult MatchScore:@""];
        if([[self splitString:textField.text splitBy:@"-"][0] intValue] > [[self splitString:textField.text splitBy:@"-"][1] intValue]){
            if(player1Score > 0)
                player1Score = player1Score - 1;
        }else{
            if(player2Score > 0)
                player2Score = player2Score - 1;
        }
        _v3txtMatchScore.text = [NSString stringWithFormat:@"%d-%d",player1Score,player2Score];
    }
    if(textField == _txtPlayer2){
        isClickOnAC = false;
    }

    textField.text = @"";
    return NO;
}

/*- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    NSLog(@"%@0000000000000000000100000000000000",event);
    [[self view] endEditing:TRUE];
    
}*/



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnBack:(id)sender {
    
}

#pragma mark - Button click event
- (IBAction)btnGame3Click:(id)sender {
    _viewGame1.hidden = YES;
    _viewGame3.hidden = NO;
    _viewGame5.hidden = YES;
    _viewGame7.hidden = YES;
    
    _viewGame1.alpha = 0;
    _viewGame3.alpha = 1;
    _viewGame5.alpha = 0;
    _viewGame7.alpha = 0;
    
    localGametype = @"3";
    
    [self restData];
    
    [_btnGame3 setImage:[UIImage imageNamed:@"radio_selected_icon"] forState:UIControlStateNormal];
    [_btnGame1 setImage:[UIImage imageNamed:@"radio_icon"] forState:UIControlStateNormal];
    [_btnGame5 setImage:[UIImage imageNamed:@"radio_icon"] forState:UIControlStateNormal];
    [_btnGame7 setImage:[UIImage imageNamed:@"radio_icon"] forState:UIControlStateNormal];

    
    _viewButton.frame = CGRectMake(_viewButton.frame.origin.x,  _viewGame3.frame.origin.y + _viewGame3.frame.size.height  , _viewButton.frame.size.width, _viewButton.frame.size.height);
    
    _scrollView.contentSize = CGSizeMake(0, _viewButton.frame.origin.y + _viewButton.frame.size.height );
}

- (IBAction)btnGame5Click:(id)sender {
    _viewGame1.hidden = YES;
    _viewGame3.hidden = YES;
    _viewGame5.hidden = NO;
    _viewGame7.hidden = YES;
    
    _viewGame1.alpha = 0;
    _viewGame3.alpha = 0;
    _viewGame5.alpha = 1;
    _viewGame7.alpha = 0;
    
    localGametype = @"5";
    [self restData];
    
    [_btnGame3 setImage:[UIImage imageNamed:@"radio_icon"] forState:UIControlStateNormal];
    [_btnGame1 setImage:[UIImage imageNamed:@"radio_icon"] forState:UIControlStateNormal];
    [_btnGame5 setImage:[UIImage imageNamed:@"radio_selected_icon"] forState:UIControlStateNormal];
    [_btnGame7 setImage:[UIImage imageNamed:@"radio_icon"] forState:UIControlStateNormal];

    
    _viewButton.frame = CGRectMake(_viewButton.frame.origin.x,  _viewGame5.frame.origin.y + _viewGame5.frame.size.height  , _viewButton.frame.size.width, _viewButton.frame.size.height);
    
    _scrollView.contentSize = CGSizeMake(0, _viewButton.frame.origin.y + _viewButton.frame.size.height );
    
}

- (IBAction)btnGame1Click:(id)sender {
    _viewGame1.hidden = NO;
    _viewGame3.hidden = YES;
    _viewGame5.hidden = YES;
    _viewGame7.hidden = YES;
    
    _viewGame1.alpha = 1;
    _viewGame3.alpha = 0;
    _viewGame5.alpha = 0;
    _viewGame7.alpha = 0;
    
    localGametype = @"1";
    
    [self restData];

    [_btnGame3 setImage:[UIImage imageNamed:@"radio_icon"] forState:UIControlStateNormal];
    [_btnGame1 setImage:[UIImage imageNamed:@"radio_selected_icon"] forState:UIControlStateNormal];
    [_btnGame5 setImage:[UIImage imageNamed:@"radio_icon"] forState:UIControlStateNormal];
    [_btnGame7 setImage:[UIImage imageNamed:@"radio_icon"] forState:UIControlStateNormal];

    
    _viewButton.frame = CGRectMake(_viewButton.frame.origin.x,  _viewGame1.frame.origin.y + _viewGame1.frame.size.height  , _viewButton.frame.size.width, _viewButton.frame.size.height);
    
    _scrollView.contentSize = CGSizeMake(0, _viewButton.frame.origin.y + _viewButton.frame.size.height );
}

- (IBAction)btnGame7Click:(id)sender {
    _viewGame1.hidden = YES;
    _viewGame3.hidden = YES;
    _viewGame5.hidden = YES;
    _viewGame7.hidden = NO;
    
    _viewGame1.alpha = 0;
    _viewGame3.alpha = 0;
    _viewGame5.alpha = 0;
    _viewGame7.alpha = 1;
    
    localGametype = @"7";
    
    [self restData];

    [_btnGame3 setImage:[UIImage imageNamed:@"radio_icon"] forState:UIControlStateNormal];
    [_btnGame1 setImage:[UIImage imageNamed:@"radio_icon"] forState:UIControlStateNormal];
    [_btnGame5 setImage:[UIImage imageNamed:@"radio_icon"] forState:UIControlStateNormal];
    [_btnGame7 setImage:[UIImage imageNamed:@"radio_selected_icon"] forState:UIControlStateNormal];
    
    _viewButton.frame = CGRectMake(_viewButton.frame.origin.x,  _viewGame7.frame.origin.y + _viewGame7.frame.size.height  , _viewButton.frame.size.width, _viewButton.frame.size.height);
    
    _scrollView.contentSize = CGSizeMake(0, _viewButton.frame.origin.y + _viewButton.frame.size.height );
}

#pragma mark - Button click event

- (IBAction)btnCalenderClick:(id)sender {
    myView = [[UIView alloc]initWithFrame:CGRectMake(btn_Calender.frame.origin.x, btn_Calender.frame.origin.x, self.view.frame.size.width/2, 150)];
    popover = [DXPopover popover];
    [popover showAtView:_tatMatchDay withContentView:_customCalendarView inView:self.view];
    
}

#pragma mark - Gesture recognizer
-(void)swipeleft:(id)sender
{
    [_customCalendarView showNextMonth];
}

-(void)swiperight:(id)sender
{
    [_customCalendarView showPreviousMonth];
}

#pragma mark - CalendarDelegate protocol conformance

-(void)dayChangedToDate:(NSDate *)selectedDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"dd-MM-yyyy EEEE";
    NSTimeZone *gmt = [NSTimeZone localTimeZone];
    [dateFormatter setTimeZone:gmt];
    NSString *timeStamp = [dateFormatter stringFromDate:selectedDate];
    NSArray *array = [timeStamp componentsSeparatedByString:@" "];
    _txtMatchDate.text = array[0];
    _tatMatchDay.text = array[1];
    [popover dismiss];
    
}

#pragma mark - Create Calendar
-(void)createCalendar{
    [self.view setBackgroundColor:[UIColor whiteColor]];
    self.title = @"All Assignment";
    _gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    _customCalendarView = [[CalendarView alloc]initWithFrame:CGRectMake(0,0, 200,( self.view.frame.size.height /3.5))];
    _customCalendarView.delegate = (id)self;
    _customCalendarView.datasource = (id)self;
    _customCalendarView.calendarDate = [NSDate date];
    _customCalendarView.monthAndDayTextColor = [UIColor whiteColor];
    _customCalendarView.dayTextColor = [UIColor grayColor];
    _customCalendarView.borderColor = [UIColor blackColor];
    _customCalendarView.dayBgColorWithData = [UIColor lightGrayColor];
    _customCalendarView.dayBgColorWithoutData = [UIColor clearColor];
    _customCalendarView.calenderWidth = 200;
    _customCalendarView.endScreenWidht = self.view.frame.size.width - 29;
    _customCalendarView.dayTxtColorWithoutData = [UIColor grayColor];
    _customCalendarView.dayTxtColorWithData = [UIColor whiteColor];
    _customCalendarView.dayTxtColorSelected = [UIColor whiteColor];
    _customCalendarView.borderColor = RGBCOLOR(159, 162, 172);
    _customCalendarView.borderWidth  = 0;
    _customCalendarView.allowsChangeMonthByDayTap = YES;
    _customCalendarView.allowsChangeMonthByButtons = YES;
    _customCalendarView.keepSelDayWhenMonthChange = YES;
    _customCalendarView.nextMonthAnimation = UIViewAnimationOptionTransitionCurlUp;
    _customCalendarView.prevMonthAnimation = UIViewAnimationOptionTransitionCurlDown;
    _customCalendarView.backgroundColor = [UIColor whiteColor];
    NSDateComponents * yearComponent = [_gregorian components:NSCalendarUnitYear fromDate:[NSDate date]];
    _currentYear = yearComponent.year;
    [myView addSubview:_customCalendarView];
}

#pragma mark - CalendarDataSource protocol conformance

-(BOOL)isDataForDate:(NSDate *)date
{
    /*if ([date compare:[NSDate date]] == NSOrderedAscending)
     return YES;
     return NO;*/
    NSDateFormatter *dateformatter = [[NSDateFormatter alloc]init];
    dateformatter.dateFormat = @"yyyy-MM-dd";
    //NSString *dateCompare = [dateformatter stringFromDate:date];
    
    return YES;
}

-(BOOL)canSwipeToDate:(NSDate *)date
{
    NSDateComponents * yearComponent = [_gregorian components:NSCalendarUnitYear fromDate:date];
    /*.tblView.frame = CGRectMake(self.tblView.frame.origin.x, self.calenderView.frame.origin.y + self.calenderView.frame.size.height + 40, self.tblView.frame.size.width, self.tblView.frame.size.height);
     [self.tblView reloadData];*/
    return (yearComponent.year == _currentYear || yearComponent.year == _currentYear+1);
}

#pragma mark - web services calling
- (void)checkScoreCorrectnessWS:(NSString *)gametype MatchScore:(NSString *)m_score
{
    [SVProgressHUD showWithStatus:@"Loading Details"];
    [[WebServiceConnector alloc]init:[NSString stringWithFormat:@"%@%@=%ld&%@=%@",URL_CheckScoreCorrectness,Gametype,(long)[gametype integerValue],Score,m_score]
                      withParameters:@{}
                          withObject:self
                        withSelector:@selector(displayScoreCorrectnessResponse:)
                      forServiceType:@"POST"
                      showDisplayMsg:@"Loading..."
                showNetworkIndicator:NO];
}

- (IBAction)displayScoreCorrectnessResponse:(id)sender
{
    [SVProgressHUD dismiss];
    if([[[sender responseDict] valueForKey:STATUS] integerValue] == 1){
        //Sucess
        NSArray *arrSplitValue;
        
        if([localGametype isEqualToString:@"1"]){
            if(txtCurrentTextfiled == _v1txtGame1){
                arrSplitValue = [[NSArray alloc]init];
                arrSplitValue = [self splitString:_v1txtGame1.text splitBy:@"-"];
                if(arrSplitValue[0] > arrSplitValue[1]){
                    _v1txtMatchScore.text = @"1-0";
                    _v1txtMatchResult.text = @"1-0";
                }else{
                    _v1txtMatchScore.text = @"0-1";
                    _v1txtMatchResult.text = @"0-1";
                }
                //Call web services for calculate member rating
                [self CalculateMemberRatingsWS:_v1txtMatchResult.text];
            }
            
        }else if([localGametype isEqualToString:@"3"]){
            [self calculatematchScoreForGame3];
        }else if([localGametype isEqualToString:@"5"]){
            [self calculatematchScoreForGame5];
        }else if([localGametype isEqualToString:@"7"]){
            [self calculatematchScoreForGame7];
        }
        
        
    }else{
        //Failed
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Alert" message:[[sender responseDict] valueForKey:@"messsage"] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        [self presentViewController:alertController animated:YES completion:nil];
        
        txtCurrentTextfiled.text = @"";
        
    }
}


- (void)CalculateMemberRatingsWS:(NSString *)matchResult {
    if (member2Id > 0) {
        [SVProgressHUD showWithStatus:@"Loading Details"];
        [[WebServiceConnector alloc]init:[NSString stringWithFormat:@"%@%@=%@&%@=%d&%@=%@",URL_CalculateMemberRatings,Member1,[DefaultsValues getStringValueFromUserDefaults_ForKey:kUserID],Member2,member2Id,Result,matchResult]
                          withParameters:@{}
                              withObject:self
                            withSelector:@selector(displayCalculateMemberRatings:)
                          forServiceType:@"POST"
                          showDisplayMsg:@"Loading..."
                    showNetworkIndicator:NO];
    }else{
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Alert" message:@"Please select player 2 first" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    
}

- (IBAction)displayCalculateMemberRatings:(id)sender
{
    [SVProgressHUD dismiss];
    if([[[sender responseDict] valueForKey:SUCCESS_STATUS] isEqualToString:@"1"]){
        dictMemberRating = [[NSMutableDictionary alloc]init];
        dictMemberRating = (NSMutableDictionary *)[sender responseDict];
        NSString *player1Rating = [[[[dictMemberRating valueForKey:@"MemberRatings"] valueForKey:@"member1"] valueForKey:@"string"] objectAtIndex:0];
        _txtPlayer1Rating.text = player1Rating;
        NSString *player2Rating = [[[[dictMemberRating valueForKey:@"MemberRatings"] valueForKey:@"member2"] valueForKey:@"string"] objectAtIndex:0];
        _txtPlayer2Rating.text = player2Rating;
        
    }else{
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Alert" message:[[sender responseDict] valueForKey:@"messsage"] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

- (void)getSearchMember:(NSString *)memberString {
    [[WebServiceConnector alloc]init:[NSString stringWithFormat:@"%@%@=%@&%@=%@",URL_MemberList,ClubId,[DefaultsValues getStringValueFromUserDefaults_ForKey:kClubID],Search,memberString]
                      withParameters:@{}
                          withObject:self
                        withSelector:@selector(getSearchMemberResponse:)
                      forServiceType:@"POST"
                      showDisplayMsg:@"Loading..."
                showNetworkIndicator:NO];
}

- (IBAction)getSearchMemberResponse:(id)sender
{
    
}

//gametype=5&memberid=39&player1=39&player2=44&match_date=04-08-2017&game_1=15-13&game_2=15-13&game_3=15-13&match_result=1-0&match_score=3-0&pos_neg_m1=add&rating_change_m1=4&ranking_m1=1081&pos_neg_m2=subtract&rating_change_m2=4&ranking_m2=969

- (void)addMatchWS {
    NSString *URL_Path = @"";
    //NSString *player1Rating = [[[[dictMemberRating valueForKey:@"MemberRatings"] valueForKey:@"member1"] valueForKey:@"string"] objectAtIndex:0];
    //NSString *player2Rating = [[[[dictMemberRating valueForKey:@"MemberRatings"] valueForKey:@"member2"] valueForKey:@"string"] objectAtIndex:0];
    NSString *player1pos_neg = [[[[dictMemberRating valueForKey:@"MemberRatings"] valueForKey:@"member1"] valueForKey:@"pos_neg"] objectAtIndex:0];
    NSString *player2pos_neg = [[[[dictMemberRating valueForKey:@"MemberRatings"] valueForKey:@"member2"] valueForKey:@"pos_neg"] objectAtIndex:0];
    NSString *player1rating_change = [[[[dictMemberRating valueForKey:@"MemberRatings"] valueForKey:@"member1"] valueForKey:@"rating_change"] objectAtIndex:0];
    NSString *player2rating_change = [[[[dictMemberRating valueForKey:@"MemberRatings"] valueForKey:@"member2"] valueForKey:@"rating_change"] objectAtIndex:0];
    NSString *player1ranking = [[[[dictMemberRating valueForKey:@"MemberRatings"] valueForKey:@"member1"] valueForKey:@"ranking"] objectAtIndex:0];
    NSString *player2ranking = [[[[dictMemberRating valueForKey:@"MemberRatings"] valueForKey:@"member2"] valueForKey:@"ranking"] objectAtIndex:0];
   
    if([localGametype isEqualToString:@"1"]){
        URL_Path = [NSString stringWithFormat:@"%@%@=%@&%@=%@&%@=%@&%@=%d&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@",URL_Addmatch,Gametype,localGametype,MemberId,[DefaultsValues getStringValueFromUserDefaults_ForKey:kUserID],Player1,[DefaultsValues getStringValueFromUserDefaults_ForKey:kUserID],Player2,member2Id,match_date,_txtMatchDate.text,game_1,_v1txtGame1.text,match_result,_v1txtMatchResult.text,match_score,_v1txtMatchScore.text,pos_neg_m1,player1pos_neg,rating_change_m1,player1rating_change,ranking_m1,player1ranking,pos_neg_m2,player2pos_neg,rating_change_m2,player2rating_change,ranking_m2,player2ranking];
    }else if([localGametype isEqualToString:@"3"]){
        URL_Path = [NSString stringWithFormat:@"%@%@=%@&%@=%@&%@=%@&%@=%d&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@",URL_Addmatch,Gametype,localGametype,MemberId,[DefaultsValues getStringValueFromUserDefaults_ForKey:kUserID],Player1,[DefaultsValues getStringValueFromUserDefaults_ForKey:kUserID],Player2,member2Id,match_date,_txtMatchDate.text,game_1,_v2txtGame1.text,game_2,_v2txtGame2.text,game_3,_v2txtGame3.text,match_result,_v2txtMatchResult.text,match_score,_v2txtMatchScore.text,pos_neg_m1,player1pos_neg,rating_change_m1,player1rating_change,ranking_m1,player1ranking,pos_neg_m2,player2pos_neg,rating_change_m2,player2rating_change,ranking_m2,player2ranking];
    }
    else if([localGametype isEqualToString:@"5"]){
        URL_Path = [NSString stringWithFormat:@"%@%@=%@&%@=%@&%@=%@&%@=%d&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@",URL_Addmatch,Gametype,localGametype,MemberId,[DefaultsValues getStringValueFromUserDefaults_ForKey:kUserID],Player1,[DefaultsValues getStringValueFromUserDefaults_ForKey:kUserID],Player2,member2Id,match_date,_txtMatchDate.text,game_1,_v3txtGame1.text,game_2,_v3txtGame2.text,game_3,_v3txtGame3.text,game_4,_v3txtGame4.text,game_5,_v3txtGame5.text,match_result,_v3txtMatchResult.text,match_score,_v3txtMatchScore.text,pos_neg_m1,player1pos_neg,rating_change_m1,player1rating_change,ranking_m1,player1ranking,pos_neg_m2,player2pos_neg,rating_change_m2,player2rating_change,ranking_m2,player2ranking];
    }else if([localGametype isEqualToString:@"7"]){
        URL_Path = [NSString stringWithFormat:@"%@%@=%@&%@=%@&%@=%@&%@=%d&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@",URL_Addmatch,Gametype,localGametype,MemberId,[DefaultsValues getStringValueFromUserDefaults_ForKey:kUserID],Player1,[DefaultsValues getStringValueFromUserDefaults_ForKey:kUserID],Player2,member2Id,match_date,_txtMatchDate.text,game_1,_txtGame1.text,game_2,_txtGame2.text,game_3,_txtGame3.text,game_4,_txtGame4.text,game_5,_txtGame5.text,game_6,_txtGame6.text,game_7,_txtGame7.text,match_result,_txtMatchResult.text,match_score,_txtMatchScore.text,pos_neg_m1,player1pos_neg,rating_change_m1,player1rating_change,ranking_m1,player1ranking,pos_neg_m2,player2pos_neg,rating_change_m2,player2rating_change,ranking_m2,player2ranking];
    }
    [[WebServiceConnector alloc]init:URL_Path
                      withParameters:@{}
                          withObject:self
                        withSelector:@selector(addMtchResponse:)
                      forServiceType:@"POST"
                      showDisplayMsg:@"Loading..."
                showNetworkIndicator:NO];
}

- (IBAction)addMtchResponse:(id)sender
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Alert" message:[[sender responseDict] valueForKey:@"messsage"] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    [self presentViewController:alertController animated:YES completion:nil];
    [self restData];

}

- (void)MemberListWS:(NSString *)searchKey {
    [[WebServiceConnector alloc]init:[NSString stringWithFormat:@"%@%@=%@&%@=%@",URL_MemberList,ClubId,[DefaultsValues getStringValueFromUserDefaults_ForKey:kClubID],Search,searchKey]
                      withParameters:@{}
                          withObject:self
                        withSelector:@selector(getMemberListResponse:)
                      forServiceType:@"POST"
                      showDisplayMsg:@"Loading..."
                showNetworkIndicator:NO];
}

- (IBAction)getMemberListResponse:(id)sender
{
    arrMeberList = [[NSMutableArray alloc]init];
    arrMeberList = (NSMutableArray *)[sender responseArray];
    NSMutableArray *arrMemberName= [[NSMutableArray alloc]init];
    for(int i = 0 ; i < [arrMeberList count]; i++){
        MemberList *obj_memberList = [arrMeberList objectAtIndex:i];
       [arrMemberName addObject:obj_memberList.name];
    }
    if([arrMeberList count] > 0){
        [self aotocompleteTextView:arrMemberName];
    }else{
        [sb.view removeFromSuperview];
    }
}

#pragma mark - Button click event
- (IBAction)btnSavaMatchClick:(id)sender {
    if(isClickOnAC){
        
        if([_txtPlayer1.text length] > 0 && [_txtPlayer2.text length] > 0 &&[_txtMatchDate.text length] > 0 && [_tatMatchDay.text length] > 0 && [_txtGame1.text length] > 0 && [_txtGame2.text length] > 0 && [_txtGame3.text length] > 0 && [_txtGame4.text length] > 0 && [_txtGame5.text length] > 0 && [_txtGame6.text length] > 0 && [_txtGame7.text length] > 0 && [_txtMatchScore.text length] > 0 && [_txtMatchResult.text length] > 0 && [localGametype isEqualToString:@"7"])
        {
            [self addMatchWS];
        }else if([_txtPlayer1.text length] > 0 && [_txtPlayer2.text length] > 0 &&[_txtMatchDate.text length] > 0 && [_tatMatchDay.text length] > 0 && [_v3txtGame1.text length] > 0 && [_v3txtGame2.text length] > 0 && [_v3txtGame3.text length] > 0 && [_v3txtGame4.text length] > 0 && [_v3txtGame5.text length] > 0 && [_v3txtMatchScore.text length] > 0 && [_v3txtMatchResult.text length] > 0 && [localGametype isEqualToString:@"5"])
        {
            [self addMatchWS];
        }else if([_txtPlayer1.text length] > 0 && [_txtPlayer2.text length] > 0 &&[_txtMatchDate.text length] > 0 && [_tatMatchDay.text length] > 0 && [_v2txtGame1.text length] > 0 && [_v2txtGame2.text length] > 0 && [_v2txtGame3.text length] > 0  && [_v2txtMatchScore.text length] > 0 && [_v2txtMatchResult.text length] > 0 && [localGametype isEqualToString:@"3"])
        {
            [self addMatchWS];
        }else if([_txtPlayer1.text length] > 0 && [_txtPlayer2.text length] > 0 &&[_txtMatchDate.text length] > 0 && [_tatMatchDay.text length] > 0 && [_v1txtGame1.text length] == 5 && [_v1txtMatchScore.text length] > 0 && [_v1txtMatchResult.text length] > 0 && [localGametype isEqualToString:@"1"])
        {
            [self addMatchWS];
        }else{
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Alert" message:@"Please fill the all field!" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:ok];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    }else{
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Alert" message:@"Please select name from list" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    
    
}

#pragma mark - Autocomplete textview

-(void)aotocompleteTextView:(NSMutableArray *)arrList{
   
    [sb.view removeFromSuperview];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:StoryBoard_InternalMatches bundle:nil];
    sb = (DropDownMenu *)[storyboard instantiateViewControllerWithIdentifier:kIDDropDownMenu];
    
    sb.dropDwonWidth = [NSString stringWithFormat:@"%f",_txtPlayer2.frame.size.width];
    sb.arrList = arrList;
    sb.xPossition = [NSString stringWithFormat:@"%f",_txtPlayer2.frame.origin.x + 35];
    sb.yPossition = [NSString stringWithFormat:@"%f",_txtPlayer2.frame.origin.y + _txtPlayer2.frame.size.height + 50];
    [sb willMoveToParentViewController:self];
    CGRect newFrame= CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height);  //Change this to adjust the position of List and size of list
    [sb.view setFrame:newFrame];
    [sb.view setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:sb.view];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"selectedListItem" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(changeValue:)
                                                 name:@"selectedListItem"
                                               object:nil];
    
    
    [self addChildViewController:sb];
    [sb didMoveToParentViewController:self];
}
#pragma mark - Dropdown delegate
- (void)changeValue:(NSNotification *)notify
{
    NSString *selItem = [[notify userInfo] valueForKey:@"item"];
    NSString *selectedRow = [[notify userInfo] valueForKey:@"id"];
    _txtPlayer2.text = selItem;
    MemberList *obj_memberList = [arrMeberList objectAtIndex:[selectedRow intValue]];
    member2Id = [obj_memberList.memberId intValue];
    [sb.view removeFromSuperview];
    
    isClickOnAC = true;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch ;
    touch = [[event allTouches] anyObject];
    
    
    if ([touch view] == self.view) //to remove the list when tapped outside
    {
        //Do what ever you want
        [sb.view removeFromSuperview];
    }
    [sb.view removeFromSuperview];
    _txtPlayer2.text = @"";
    
}

#pragma mark - calaculate match score and match result
-(void)calculatematchScoreForGame3{
    
    NSArray *arrSplitValue;
    if(txtCurrentTextfiled == _v2txtGame1 && [_v2txtGame1.text length] > 0){
        arrSplitValue = [[NSArray alloc]init];
        arrSplitValue = [self splitString:_v2txtGame1.text splitBy:@"-"];
        if([arrSplitValue[0] intValue] > [arrSplitValue[1] intValue]){
            player1Score += 1;
        }else{
            player2Score += 1;
        }
    }
    else if(txtCurrentTextfiled == _v2txtGame2 && [_v2txtGame2.text length] > 0){
        arrSplitValue = [[NSArray alloc]init];
        arrSplitValue = [self splitString:_v2txtGame2.text splitBy:@"-"];
        if([arrSplitValue[0] intValue] > [arrSplitValue[1] intValue]){
            player1Score += 1;
        }else{
            player2Score += 1;
        }
    }
    
    _v2txtMatchScore.text = [NSString stringWithFormat:@"%d-%d",player1Score,player2Score];
    
    if([_v2txtMatchScore.text length] == 3 && ([[self splitString:_v2txtMatchScore.text splitBy:@"-"][0] isEqualToString:@"2"] || [[self splitString:_v2txtMatchScore.text splitBy:@"-"][1] isEqualToString:@"2"]) ){
        _v2txtGame3.enabled = false;
        [self setMatchResult:_v2txtMatchResult MatchScore:_v2txtMatchScore.text];
    }else{
        _v2txtGame3.enabled = true;
        [self setMatchResult:_v2txtMatchResult MatchScore:@""];
        if(txtCurrentTextfiled == _v2txtGame3  && [_v2txtGame3.text length] > 0){
            arrSplitValue = [[NSArray alloc]init];
            arrSplitValue = [self splitString:_v2txtGame3.text splitBy:@"-"];
            if([arrSplitValue[0] intValue] > [arrSplitValue[1] intValue]){
                player1Score += 1;
            }else{
                player2Score += 1;
            }
             _v2txtMatchScore.text = [NSString stringWithFormat:@"%d-%d",player1Score,player2Score];
            [self setMatchResult:_v2txtMatchResult MatchScore:_v2txtMatchScore.text];
        }
        
    }
   
}

-(void)calculatematchScoreForGame5{
    
    NSArray *arrSplitValue;
    if(txtCurrentTextfiled == _v3txtGame1 && [_v3txtGame1.text length] > 0){
        arrSplitValue = [[NSArray alloc]init];
        arrSplitValue = [self splitString:_v3txtGame1.text splitBy:@"-"];
        if([arrSplitValue[0] intValue] > [arrSplitValue[1] intValue]){
            player1Score += 1;
        }else{
            player2Score += 1;
        }
    }
    else if(txtCurrentTextfiled == _v3txtGame2 && [_v3txtGame2.text length] > 0){
        arrSplitValue = [[NSArray alloc]init];
        arrSplitValue = [self splitString:_v3txtGame2.text splitBy:@"-"];
        if([arrSplitValue[0] intValue] > [arrSplitValue[1] intValue]){
            player1Score += 1;
        }else{
            player2Score += 1;
        }
    }
    else if(txtCurrentTextfiled == _v3txtGame3 && [_v3txtGame3.text length] > 0){
        arrSplitValue = [[NSArray alloc]init];
        arrSplitValue = [self splitString:_v3txtGame3.text splitBy:@"-"];
        if([arrSplitValue[0] intValue] > [arrSplitValue[1] intValue]){
            player1Score += 1;
        }else{
            player2Score += 1;
        }
    }
    
    _v3txtMatchScore.text = [NSString stringWithFormat:@"%d-%d",player1Score,player2Score];
    
    if([_v3txtMatchScore.text length] == 3 && ([[self splitString:_v3txtMatchScore.text splitBy:@"-"][0] isEqualToString:@"3"] || [[self splitString:_v3txtMatchScore.text splitBy:@"-"][1] isEqualToString:@"3"]) ){
        _v3txtGame4.enabled = false;
        _v3txtGame5.enabled = false;
        [self setMatchResult:_v3txtMatchResult MatchScore:_v3txtMatchScore.text];
    }else{
        _v3txtGame4.enabled = true;
        _v3txtGame5.enabled = true;
        [self setMatchResult:_v3txtMatchResult MatchScore:@""];
        if(txtCurrentTextfiled == _v3txtGame4  && [_v3txtGame4.text length] > 0){
            arrSplitValue = [[NSArray alloc]init];
            arrSplitValue = [self splitString:_v3txtGame4.text splitBy:@"-"];
            if([arrSplitValue[0] intValue] > [arrSplitValue[1] intValue]){
                player1Score += 1;
            }else{
                player2Score += 1;
            }
            _v3txtMatchScore.text = [NSString stringWithFormat:@"%d-%d",player1Score,player2Score];
            //[self setMatchResult:_v3txtMatchResult MatchScore:_v3txtMatchScore.text];
        }
        else if(txtCurrentTextfiled == _v3txtGame5  && [_v3txtGame5.text length] > 0){
            arrSplitValue = [[NSArray alloc]init];
            arrSplitValue = [self splitString:_v3txtGame5.text splitBy:@"-"];
            if([arrSplitValue[0] intValue] > [arrSplitValue[1] intValue]){
                player1Score += 1;
            }else{
                player2Score += 1;
            }
            _v3txtMatchScore.text = [NSString stringWithFormat:@"%d-%d",player1Score,player2Score];
            [self setMatchResult:_v3txtMatchResult MatchScore:_v3txtMatchScore.text];
        }
        
    }
    
}

-(void)calculatematchScoreForGame7{
    
    NSArray *arrSplitValue;
    if(txtCurrentTextfiled == _txtGame1 && [_txtGame1.text length] > 0){
        arrSplitValue = [[NSArray alloc]init];
        arrSplitValue = [self splitString:_txtGame1.text splitBy:@"-"];
        if([arrSplitValue[0] intValue] > [arrSplitValue[1] intValue]){
            player1Score += 1;
        }else{
            player2Score += 1;
        }
    }
    else if(txtCurrentTextfiled == _txtGame2 && [_txtGame2.text length] > 0){
        arrSplitValue = [[NSArray alloc]init];
        arrSplitValue = [self splitString:_txtGame2.text splitBy:@"-"];
        if([arrSplitValue[0] intValue] > [arrSplitValue[1] intValue]){
            player1Score += 1;
        }else{
            player2Score += 1;
        }
    }
    else if(txtCurrentTextfiled == _txtGame3 && [_txtGame3.text length] > 0){
        arrSplitValue = [[NSArray alloc]init];
        arrSplitValue = [self splitString:_txtGame3.text splitBy:@"-"];
        if([arrSplitValue[0] intValue] > [arrSplitValue[1] intValue]){
            player1Score += 1;
        }else{
            player2Score += 1;
        }
    }
    else if(txtCurrentTextfiled == _txtGame4 && [_txtGame4.text length] > 0){
        arrSplitValue = [[NSArray alloc]init];
        arrSplitValue = [self splitString:_txtGame4.text splitBy:@"-"];
        if([arrSplitValue[0] intValue] > [arrSplitValue[1] intValue]){
            player1Score += 1;
        }else{
            player2Score += 1;
        }
    }
    
    _txtMatchScore.text = [NSString stringWithFormat:@"%d-%d",player1Score,player2Score];
    
    if([_txtMatchScore.text length] == 3 && ([[self splitString:_txtMatchScore.text splitBy:@"-"][0] isEqualToString:@"4"] || [[self splitString:_txtMatchScore.text splitBy:@"-"][1] isEqualToString:@"4"]) ){
        _txtGame5.enabled = false;
        _txtGame6.enabled = false;
        _txtGame7.enabled = false;
        [self setMatchResult:_txtMatchResult MatchScore:_txtMatchScore.text];
    }else{
        _txtGame5.enabled = true;
        _txtGame6.enabled = true;
        _txtGame7.enabled = true;
        [self setMatchResult:_v3txtMatchResult MatchScore:@""];
        if(txtCurrentTextfiled == _txtGame5  && [_txtGame5.text length] > 0){
            arrSplitValue = [[NSArray alloc]init];
            arrSplitValue = [self splitString:_txtGame5.text splitBy:@"-"];
            if([arrSplitValue[0] intValue] > [arrSplitValue[1] intValue]){
                player1Score += 1;
            }else{
                player2Score += 1;
            }
            _txtMatchScore.text = [NSString stringWithFormat:@"%d-%d",player1Score,player2Score];
            //[self setMatchResult:_v3txtMatchResult MatchScore:_v3txtMatchScore.text];
            if([_txtMatchScore.text length] == 3 && ([[self splitString:_txtMatchScore.text splitBy:@"-"][0] isEqualToString:@"4"] || [[self splitString:_txtMatchScore.text splitBy:@"-"][1] isEqualToString:@"4"]) ){
                _txtGame6.enabled = false;
                _txtGame7.enabled = false;
                [self setMatchResult:_txtMatchResult MatchScore:_txtMatchScore.text];
            }

        }
        else if(txtCurrentTextfiled == _txtGame6  && [_txtGame6.text length] > 0){
            arrSplitValue = [[NSArray alloc]init];
            arrSplitValue = [self splitString:_txtGame6.text splitBy:@"-"];
            if([arrSplitValue[0] intValue] > [arrSplitValue[1] intValue]){
                player1Score += 1;
            }else{
                player2Score += 1;
            }
            _txtMatchScore.text = [NSString stringWithFormat:@"%d-%d",player1Score,player2Score];
            if([_txtMatchScore.text length] == 3 && ([[self splitString:_txtMatchScore.text splitBy:@"-"][0] isEqualToString:@"4"] || [[self splitString:_txtMatchScore.text splitBy:@"-"][1] isEqualToString:@"4"]) ){
               
                _txtGame7.enabled = false;
                [self setMatchResult:_txtMatchResult MatchScore:_txtMatchScore.text];
            }
           
        }
        else if(txtCurrentTextfiled == _txtGame7  && [_txtGame7.text length] > 0){
            arrSplitValue = [[NSArray alloc]init];
            arrSplitValue = [self splitString:_txtGame7.text splitBy:@"-"];
            if([arrSplitValue[0] intValue] > [arrSplitValue[1] intValue]){
                player1Score += 1;
            }else{
                player2Score += 1;
            }
            _txtMatchScore.text = [NSString stringWithFormat:@"%d-%d",player1Score,player2Score];
            [self setMatchResult:_txtMatchResult MatchScore:_txtMatchScore.text];
        }

        
    }
    
}

-(void)setMatchResult:(UITextField*)txtField MatchScore:(NSString *)matchScore{
    if([matchScore length] > 0){
        NSArray *arrSplitScore = [self splitString:matchScore splitBy:@"-"];
        if([arrSplitScore[0] intValue] > [arrSplitScore[1] intValue]){
            txtField.text = @"1-0";
        }else{
            txtField.text = @"0-1";
        }
        
        //Call web services for calculate member rating
        [self CalculateMemberRatingsWS:txtField.text];
    }else{
        txtField.text = @"";
    }
}
#pragma mark - reset all textfiled
-(void)restData{
    isClickOnAC = false;
    
    player1Score = 0;
    player2Score = 0;
    
    _txtGame1.text = @"";
    _txtGame2.text = @"";
    _txtGame3.text = @"";
    _txtGame4.text = @"";
    _txtGame5.text = @"";
    _txtGame6.text = @"";
    _txtGame7.text = @"";
    _txtMatchResult.text = @"";
    _txtMatchResult.text = @"";
    
    _v3txtGame1.text = @"";
    _v3txtGame2.text = @"";
    _v3txtGame3.text = @"";
    _v3txtGame4.text = @"";
    _v3txtGame5.text = @"";
    _v3txtMatchScore.text = @"";
    _v3txtMatchResult.text = @"";
    
    _v2txtGame1.text = @"";
    _v2txtGame2.text = @"";
    _v2txtGame3.text = @"";
    _v2txtMatchScore.text = @"";
    _v2txtMatchResult.text = @"";
    
    _v1txtGame1.text = @"";
    _v1txtMatchScore.text = @"";
    _v1txtMatchResult.text = @"";
    
}


@end
