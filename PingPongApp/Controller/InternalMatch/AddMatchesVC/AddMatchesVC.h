//
//  AddMatchesVC.h
//  PingPongApp
//
//  Created by keval vasaniya on 04/08/17.
//  Copyright © 2017 keval vasaniya. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseVC.h"
#import "CalendarView.h"

@interface AddMatchesVC : BaseVC <UITextFieldDelegate>
- (IBAction)btnBack:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *viewGame1;
@property (weak, nonatomic) IBOutlet UIView *viewGame3;
@property (weak, nonatomic) IBOutlet UIView *viewGame5;
@property (weak, nonatomic) IBOutlet UIView *viewGame7;
@property (weak, nonatomic) IBOutlet UIView *viewButton;

@property (weak, nonatomic) IBOutlet UIButton *btnGame1;
@property (weak, nonatomic) IBOutlet UIButton *btnGame3;
@property (weak, nonatomic) IBOutlet UIButton *btnGame7;
@property (weak, nonatomic) IBOutlet UIButton *btnGame5;






- (IBAction)btnGame3Click:(id)sender;
- (IBAction)btnGame5Click:(id)sender;
- (IBAction)btnGame1Click:(id)sender;
- (IBAction)btnGame7Click:(id)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UITextField *txtPlayer1;
@property (weak, nonatomic) IBOutlet UITextField *txtPlayer1Rating;
@property (weak, nonatomic) IBOutlet UITextField *txtPlayer2;



@property (weak, nonatomic) IBOutlet UITextField *txtPlayer2Rating;
@property (weak, nonatomic) IBOutlet UITextField *txtMatchDate;
@property (weak, nonatomic) IBOutlet UITextField *tatMatchDay;
@property (weak, nonatomic) IBOutlet UITextField *txtGame1;
@property (weak, nonatomic) IBOutlet UITextField *txtGame2;
@property (weak, nonatomic) IBOutlet UITextField *txtGame3;
@property (weak, nonatomic) IBOutlet UITextField *txtGame4;
@property (weak, nonatomic) IBOutlet UITextField *txtGame5;
@property (weak, nonatomic) IBOutlet UITextField *txtGame6;
@property (weak, nonatomic) IBOutlet UITextField *txtGame7;
@property (weak, nonatomic) IBOutlet UITextField *txtMatchScore;
@property (weak, nonatomic) IBOutlet UITextField *txtMatchResult;
@property (weak, nonatomic) IBOutlet UITextView *txtPlayer1Comment;
@property (weak, nonatomic) IBOutlet UITextView *txtPlayer2Comment;

@property (weak, nonatomic) IBOutlet UITextView *view2txtPlayer1Comment;
@property (weak, nonatomic) IBOutlet UITextView *view2txtPlayer2Comment;

@property (weak, nonatomic) IBOutlet UITextView *view3txtPlayer1Comment;
@property (weak, nonatomic) IBOutlet UITextView *view3txtPlayer2Comment;

@property (weak, nonatomic) IBOutlet UITextView *view4txtPlayer1Comment;
@property (weak, nonatomic) IBOutlet UITextView *view4txtPlayer2Comment;
@property (weak, nonatomic) IBOutlet UITextField *v3txtGame1;
@property (weak, nonatomic) IBOutlet UITextField *v3txtGame3;
@property (weak, nonatomic) IBOutlet UITextField *v3txtGame5;
@property (weak, nonatomic) IBOutlet UITextField *v3txtGame2;
@property (weak, nonatomic) IBOutlet UITextField *v3txtGame4;
@property (weak, nonatomic) IBOutlet UITextField *v3txtMatchScore;
@property (weak, nonatomic) IBOutlet UITextField *v3txtMatchResult;

@property (weak, nonatomic) IBOutlet UITextField *v2txtGame1;
@property (weak, nonatomic) IBOutlet UITextField *v2txtGame3;
@property (weak, nonatomic) IBOutlet UITextField *v2txtGame2;
@property (weak, nonatomic) IBOutlet UITextField *v2txtMatchScore;
@property (weak, nonatomic) IBOutlet UITextField *v2txtMatchResult;

@property (weak, nonatomic) IBOutlet UITextField *v1txtGame1;
@property (weak, nonatomic) IBOutlet UITextField *v1txtMatchScore;
@property (weak, nonatomic) IBOutlet UITextField *v1txtMatchResult;
@property (weak, nonatomic) IBOutlet UIButton *btn_Calenderclick;

- (IBAction)btnSavaMatchClick:(id)sender;



@end
