//
//  AppDelegate.h
//  PingPongApp
//
//  Created by keval vasaniya on 31/07/17.
//  Copyright © 2017 keval vasaniya. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMDrawerController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic)  MMDrawerController *drawerController;
@property (strong, nonatomic) UINavigationController *navigationControllerCenter;
@property (strong, nonatomic) UINavigationController *navigationControllerLeft;


@end

