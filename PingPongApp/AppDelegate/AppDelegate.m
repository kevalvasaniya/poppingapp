//
//  AppDelegate.m
//  PingPongApp
//
//  Created by keval vasaniya on 31/07/17.
//  Copyright © 2017 keval vasaniya. All rights reserved.
//

#import "AppDelegate.h"
#import "SidebarVC.h"
#import "Constant.h"
#import "HomeVC.h"
#import "BaseVC.h"
#import "LoginVC.h"


@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    /*SidebarVC *objSidebarVC = loadViewController(StoryBoard_Home, kIDSidebarVC);
    HomeVC *objScreen1VC = loadViewController(StoryBoard_Home, kIDHomeVC);
    
    self.navigationControllerCenter = [[UINavigationController alloc] initWithRootViewController:objScreen1VC];
    self.navigationControllerLeft = [[UINavigationController alloc] initWithRootViewController:objSidebarVC];
    
    self.drawerController = [[MMDrawerController alloc] initWithCenterViewController:self.navigationControllerCenter
                                                            leftDrawerViewController:self.navigationControllerLeft
                                                           rightDrawerViewController:nil];
    
    [self.drawerController setMaximumLeftDrawerWidth:200];
    
    [self.drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
    // [self.drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModePanningNavigationBar]; //Uncomment this line if do not want to open sidebar on swipe
    [self.drawerController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
    
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [self.window setRootViewController:self.drawerController];*/
    
    [self setInitialViews];

    
    return YES;
}

-(void) setInitialViews
{
    // set view controllers
    LoginVC *main = loadViewController(StoryBoard_Main,kIDLoginVC);
    [self.window setRootViewController:main];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
