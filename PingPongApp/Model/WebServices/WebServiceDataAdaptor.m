//
//  WebHelper.m
//  SQLExample
//
//  Created by iMac on 17/03/14.
//  Copyright (c) 2014 Narola. All rights reserved.
//
//[[FamousFoodWS alloc]initWithDictionary:allvalues];
//[[classname alloc]initWithDictionary:allvalues];
//[[[NSClassFromString(classname) alloc] init] initWithDictionary:allvalues]

#import "WebServiceDataAdaptor.h"
#import "WSConstant.h"
#import "DefaultsValues.h"
#import <objc/runtime.h>
#import "NSString+Extensions.h"
#import "Function.h"

@implementation WebServiceDataAdaptor

@synthesize arrParsedData;

-(NSArray *) autoParse:(NSDictionary *) allValues forServiceName:(NSString *)requestURL
{
    arrParsedData = [NSArray new];
 
    if (isService_Detail(URL_LeftMenu))
    {
        arrParsedData = [self processJSONData:allValues
                                     forClass:LeftMenuClass
                                    forEntity:LeftMenuEntity
                                  withJSONKey:LeftMenuKey];
        
    }else if (isService_Detail(URL_Login))
    {
        arrParsedData = [self processJSONData:allValues
                                     forClass:LoginClass
                                    forEntity:LoginEntity
                                  withJSONKey:LoginKey];
        
    }else if (isService_Detail(URL_AllClubMatches))
    {
        arrParsedData = [self processJSONData:allValues
                                     forClass:AllClubMatchesClass
                                    forEntity:AllClubMatchesEntity
                                  withJSONKey:AllClubMatchesKey];
        
    }else if (isService_Detail(URL_CalculateMemberRatings))
    {
        arrParsedData = [self processJSONData:allValues
                                     forClass:CalculateMemberRatingsClass
                                    forEntity:CalculateMemberRatingsEntity
                                  withJSONKey:CalculateMemberRatingsKey];
        
    }else if (isService_Detail(URL_MyMatchList)){
        arrParsedData = [self processJSONData:allValues
                                     forClass:AllMyMatchesClass
                                    forEntity:AllMyMatchesEntity
                                  withJSONKey:AllMyMatchesKey];
    }
    else if(isService_Detail(URL_MemberList)){
        arrParsedData = [self processJSONData:allValues
                                     forClass:MemberslistClass
                                    forEntity:MemberslistEntity
                                  withJSONKey:MemberslistKey];
    }
    /*else if (isService_Detail(URL_CheckScoreCorrectness))
    {
        arrParsedData = [self processJSONData:allValues
                                     forClass:CheckScoreCorrectnessClass
                                    forEntity:CheckScoreCorrectnessEntity
                                  withJSONKey:CheckScoreCorrectnessKey];
        
    }*/

    return arrParsedData;
}

#pragma mark - Helper Method
-(void)processJSONToUserDefaults:(NSDictionary *)dict withJSONKeys:(NSMutableArray *)json_Keys
{
    /** this method will save the value of single key to user default. Modify this method to get multiple inner values of dict key or to return string from function **/
    for(int i =0;i<[json_Keys count];i++)
    {
       [DefaultsValues setStringValueToUserDefaults:[Function getStringForKey:[json_Keys objectAtIndex:i] fromDictionary:dict] ForKey:[json_Keys objectAtIndex:i]];
    }
}

-(NSArray *) processJSONData: (NSDictionary *)dict forClass:(NSString *)classname forEntity:(NSString *)entityname withJSONKey:(NSString *)json_Key
{
    NSLog(@"**************************^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
    NSMutableArray *arrProcessedData = [NSMutableArray array];
    //[CoreDataAdaptor deleteDataInCoreDB:entityname];
    for(int i =0;i<[[dict objectForKey:json_Key] count];i++)
    {
        NSDictionary *allvalues = [[dict objectForKey:json_Key] objectAtIndex:i];
        id objClass = [[[NSClassFromString(classname) alloc] init] initWithDictionary:allvalues];
        [arrProcessedData addObject:objClass];

        //if(![Function stringIsEmpty:entityname])
        //[CoreDataAdaptor SaveDataInCoreDB:[self processObjectForCoreData:objClass] forEntity:entityname];
    }
    return arrProcessedData;
}

-(NSDictionary *)processObjectForCoreData:(id)obj
{
    NSArray *aVoidArray =@[@"NSDate"];
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    unsigned count;
    objc_property_t *properties = class_copyPropertyList([obj class], &count);
    for (int i = 0; i < count; i++)
    {
        NSString *key = [NSString stringWithUTF8String:property_getName(properties[i])];
        if (![aVoidArray containsObject: key] )
        {
            if ([obj valueForKey:key]!=nil)
            {
                [dict setObject:[obj valueForKey:key] forKey:key];
            }
        }
    }
    return dict;
}

@end
