//
//  List.h
//
//  Created by   on 03/08/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface List : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSArray *child;
@property (nonatomic, strong) NSString *main;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
