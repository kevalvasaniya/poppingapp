//
//  List.m
//
//  Created by   on 03/08/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "List.h"


NSString *const kListChild = @"child";
NSString *const kListMain = @"main";


@interface List ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation List

@synthesize child = _child;
@synthesize main = _main;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.child = [self objectOrNilForKey:kListChild fromDictionary:dict];
            self.main = [self objectOrNilForKey:kListMain fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    NSMutableArray *tempArrayForChild = [NSMutableArray array];
    
    for (NSObject *subArrayObject in self.child) {
        if ([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForChild addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForChild addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForChild] forKey:kListChild];
    [mutableDict setValue:self.main forKey:kListMain];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.child = [aDecoder decodeObjectForKey:kListChild];
    self.main = [aDecoder decodeObjectForKey:kListMain];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_child forKey:kListChild];
    [aCoder encodeObject:_main forKey:kListMain];
}

- (id)copyWithZone:(NSZone *)zone {
    List *copy = [[List alloc] init];
    
    
    
    if (copy) {

        copy.child = [self.child copyWithZone:zone];
        copy.main = [self.main copyWithZone:zone];
    }
    
    return copy;
}


@end
