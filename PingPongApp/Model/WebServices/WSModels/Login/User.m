//
//  User.m
//
//  Created by   on 13/08/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "User.h"


NSString *const kUserClubLogo = @"club_logo";
NSString *const kUserId = @"id";
NSString *const kUserClubName = @"club_name";
NSString *const kUserUnionNumber = @"union_number";
NSString *const kUserFacebookUrl = @"facebook_url";
NSString *const kUserMobile = @"mobile";
NSString *const kUserClubId = @"club_id";
NSString *const kUserLastname = @"lastname";
NSString *const kUserDateBirth = @"date_birth";
NSString *const kUserFirstname = @"firstname";
NSString *const kUserAppCode = @"app_code";
NSString *const kUserAddress = @"address";
NSString *const kUserPostalCode = @"postal_code";
NSString *const kUserEmail = @"email";
NSString *const kUserGender = @"gender";
NSString *const kUserPhoto = @"photo";


@interface User ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation User

@synthesize clubLogo = _clubLogo;
@synthesize userIdentifier = _userIdentifier;
@synthesize clubName = _clubName;
@synthesize unionNumber = _unionNumber;
@synthesize facebookUrl = _facebookUrl;
@synthesize mobile = _mobile;
@synthesize clubId = _clubId;
@synthesize lastname = _lastname;
@synthesize dateBirth = _dateBirth;
@synthesize firstname = _firstname;
@synthesize appCode = _appCode;
@synthesize address = _address;
@synthesize postalCode = _postalCode;
@synthesize email = _email;
@synthesize gender = _gender;
@synthesize photo = _photo;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.clubLogo = [self objectOrNilForKey:kUserClubLogo fromDictionary:dict];
            self.userIdentifier = [self objectOrNilForKey:kUserId fromDictionary:dict];
            self.clubName = [self objectOrNilForKey:kUserClubName fromDictionary:dict];
            self.unionNumber = [self objectOrNilForKey:kUserUnionNumber fromDictionary:dict];
            self.facebookUrl = [self objectOrNilForKey:kUserFacebookUrl fromDictionary:dict];
            self.mobile = [self objectOrNilForKey:kUserMobile fromDictionary:dict];
            self.clubId = [self objectOrNilForKey:kUserClubId fromDictionary:dict];
            self.lastname = [self objectOrNilForKey:kUserLastname fromDictionary:dict];
            self.dateBirth = [self objectOrNilForKey:kUserDateBirth fromDictionary:dict];
            self.firstname = [self objectOrNilForKey:kUserFirstname fromDictionary:dict];
            self.appCode = [self objectOrNilForKey:kUserAppCode fromDictionary:dict];
            self.address = [self objectOrNilForKey:kUserAddress fromDictionary:dict];
            self.postalCode = [self objectOrNilForKey:kUserPostalCode fromDictionary:dict];
            self.email = [self objectOrNilForKey:kUserEmail fromDictionary:dict];
            self.gender = [self objectOrNilForKey:kUserGender fromDictionary:dict];
            self.photo = [self objectOrNilForKey:kUserPhoto fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.clubLogo forKey:kUserClubLogo];
    [mutableDict setValue:self.userIdentifier forKey:kUserId];
    [mutableDict setValue:self.clubName forKey:kUserClubName];
    [mutableDict setValue:self.unionNumber forKey:kUserUnionNumber];
    [mutableDict setValue:self.facebookUrl forKey:kUserFacebookUrl];
    [mutableDict setValue:self.mobile forKey:kUserMobile];
    [mutableDict setValue:self.clubId forKey:kUserClubId];
    [mutableDict setValue:self.lastname forKey:kUserLastname];
    [mutableDict setValue:self.dateBirth forKey:kUserDateBirth];
    [mutableDict setValue:self.firstname forKey:kUserFirstname];
    [mutableDict setValue:self.appCode forKey:kUserAppCode];
    [mutableDict setValue:self.address forKey:kUserAddress];
    [mutableDict setValue:self.postalCode forKey:kUserPostalCode];
    [mutableDict setValue:self.email forKey:kUserEmail];
    [mutableDict setValue:self.gender forKey:kUserGender];
    [mutableDict setValue:self.photo forKey:kUserPhoto];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.clubLogo = [aDecoder decodeObjectForKey:kUserClubLogo];
    self.userIdentifier = [aDecoder decodeObjectForKey:kUserId];
    self.clubName = [aDecoder decodeObjectForKey:kUserClubName];
    self.unionNumber = [aDecoder decodeObjectForKey:kUserUnionNumber];
    self.facebookUrl = [aDecoder decodeObjectForKey:kUserFacebookUrl];
    self.mobile = [aDecoder decodeObjectForKey:kUserMobile];
    self.clubId = [aDecoder decodeObjectForKey:kUserClubId];
    self.lastname = [aDecoder decodeObjectForKey:kUserLastname];
    self.dateBirth = [aDecoder decodeObjectForKey:kUserDateBirth];
    self.firstname = [aDecoder decodeObjectForKey:kUserFirstname];
    self.appCode = [aDecoder decodeObjectForKey:kUserAppCode];
    self.address = [aDecoder decodeObjectForKey:kUserAddress];
    self.postalCode = [aDecoder decodeObjectForKey:kUserPostalCode];
    self.email = [aDecoder decodeObjectForKey:kUserEmail];
    self.gender = [aDecoder decodeObjectForKey:kUserGender];
    self.photo = [aDecoder decodeObjectForKey:kUserPhoto];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_clubLogo forKey:kUserClubLogo];
    [aCoder encodeObject:_userIdentifier forKey:kUserId];
    [aCoder encodeObject:_clubName forKey:kUserClubName];
    [aCoder encodeObject:_unionNumber forKey:kUserUnionNumber];
    [aCoder encodeObject:_facebookUrl forKey:kUserFacebookUrl];
    [aCoder encodeObject:_mobile forKey:kUserMobile];
    [aCoder encodeObject:_clubId forKey:kUserClubId];
    [aCoder encodeObject:_lastname forKey:kUserLastname];
    [aCoder encodeObject:_dateBirth forKey:kUserDateBirth];
    [aCoder encodeObject:_firstname forKey:kUserFirstname];
    [aCoder encodeObject:_appCode forKey:kUserAppCode];
    [aCoder encodeObject:_address forKey:kUserAddress];
    [aCoder encodeObject:_postalCode forKey:kUserPostalCode];
    [aCoder encodeObject:_email forKey:kUserEmail];
    [aCoder encodeObject:_gender forKey:kUserGender];
    [aCoder encodeObject:_photo forKey:kUserPhoto];
}

- (id)copyWithZone:(NSZone *)zone {
    User *copy = [[User alloc] init];
    
    
    
    if (copy) {

        copy.clubLogo = [self.clubLogo copyWithZone:zone];
        copy.userIdentifier = [self.userIdentifier copyWithZone:zone];
        copy.clubName = [self.clubName copyWithZone:zone];
        copy.unionNumber = [self.unionNumber copyWithZone:zone];
        copy.facebookUrl = [self.facebookUrl copyWithZone:zone];
        copy.mobile = [self.mobile copyWithZone:zone];
        copy.clubId = [self.clubId copyWithZone:zone];
        copy.lastname = [self.lastname copyWithZone:zone];
        copy.dateBirth = [self.dateBirth copyWithZone:zone];
        copy.firstname = [self.firstname copyWithZone:zone];
        copy.appCode = [self.appCode copyWithZone:zone];
        copy.address = [self.address copyWithZone:zone];
        copy.postalCode = [self.postalCode copyWithZone:zone];
        copy.email = [self.email copyWithZone:zone];
        copy.gender = [self.gender copyWithZone:zone];
        copy.photo = [self.photo copyWithZone:zone];
    }
    
    return copy;
}


@end
