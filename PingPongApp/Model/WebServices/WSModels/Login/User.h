//
//  User.h
//
//  Created by   on 13/08/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface User : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *clubLogo;
@property (nonatomic, strong) NSString *userIdentifier;
@property (nonatomic, strong) NSString *clubName;
@property (nonatomic, strong) NSString *unionNumber;
@property (nonatomic, strong) NSString *facebookUrl;
@property (nonatomic, strong) NSString *mobile;
@property (nonatomic, strong) NSString *clubId;
@property (nonatomic, strong) NSString *lastname;
@property (nonatomic, strong) NSString *dateBirth;
@property (nonatomic, strong) NSString *firstname;
@property (nonatomic, strong) NSString *appCode;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *postalCode;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *gender;
@property (nonatomic, strong) NSString *photo;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
