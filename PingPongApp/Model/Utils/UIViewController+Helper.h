//
//  UIViewController+Helper.h
//
//  Created by Milan Gupta on 04/03/15.
//  Copyright (c) 2015 C116. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <AVFoundation/AVFoundation.h>



@interface UIViewController (Helper)

- (void) setupLeftMenuButton;


#pragma mark - Generate Random String
-(NSString*)generateRandomString:(int)num ;

#pragma mark - Base 64 for UIImage
- (NSString *)encodeImageToBase64String:(UIImage *)image;

#pragma mark - Base 64 for Media
- (NSString *)encodeVideoToBase64String:(NSData *)video;
- (NSString *)encodeAudioToBase64String:(NSData *)audio;

#pragma mark - image resize
- (UIImage *)scaleAndRotateImage:(UIImage *)image;

#pragma mark - audio functions
-(NSData *)extractDataForAsset:(AVURLAsset *)songAsset;

#pragma mark - video functions
@end
