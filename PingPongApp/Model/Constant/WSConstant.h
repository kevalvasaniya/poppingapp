//
//  WSConstant.h
//  Hinter
//
//  Created by C218 on 08/11/16.
//  Copyright © 2016 C218. All rights reserved.
//

#ifndef WSConstant_h
#define WSConstant_h

#define isService( key ) \
[requestURL isEqualToString: key ]

#define isService_Detail( key ) \
[requestURL localizedCaseInsensitiveContainsString:key]

#define ENV_DEV         0
#define ENV_PROD        1

#define Server_Type ENV_PROD



//#if Server_Type == ENV_DEV
//
//#define Server_URL      @"http://localhost:8080"
#define  ServerPath     @"https://members.pingpongapp.eu/api/"
//
//#else

#define Server_URL   @"https://members.pingpongapp.eu/api/"


//#define InteractionImagePath [NSString stringWithFormat:@"%@/pg/NappsisWS/Upload/Interaction/interaction_image/",Server_URL]

//#endif

#define BASE_URL [NSString stringWithFormat:@"%@%@",Server_URL,ServerPath]

#define ShowNetworkIndicator(XXX) [UIApplication sharedApplication].networkActivityIndicatorVisible = XXX;
#define NetworkLost @"The network connection was lost."
#define NoNetwork @"No internet connection."
#define UserProfileImage(name) [NSString stringWithFormat:@"%@%@",BASE_URL,name]
#define NoteImage(name) [NSString stringWithFormat:@"%@note_images/%@",BASE_URL,name]

#define URL_LeftMenu [NSString stringWithFormat:@"https://members.pingpongapp.eu/api/ppa_LeftMenulist?"]
#define URL_Login [NSString stringWithFormat:@"https://members.pingpongapp.eu/api/ppa_Login?"]
#define URL_AllClubMatches [NSString stringWithFormat:@"https://members.pingpongapp.eu/api/ppa_getAllClubMatches?"]
#define URL_CheckScoreCorrectness [NSString stringWithFormat:@"http://members.pingpongapp.eu/api/ppa_CheckScoreCorrectness?"]
#define URL_CalculateMemberRatings [NSString stringWithFormat:@"http://members.pingpongapp.eu/api/ppa_CalculateMemberRatings?"]
#define URL_Addmatch [NSString stringWithFormat:@"http://members.pingpongapp.eu/api/ppa_Addmatch?"]
#define URL_MemberList [NSString stringWithFormat:@"http://members.pingpongapp.eu/api/ppa_Memberslist?"]
#define URL_MyMatchList [NSString stringWithFormat:@"https://members.pingpongapp.eu/api/ppa_Matcheslist?"]




#pragma mark - Class,Json Key and Message

#define WebserviceDialogMsg @"Please wait"

#define LeftMenuClass @"List"
#define LeftMenuEntity @""
#define LeftMenuKey @"List"
#define LeftMenuMsg @"Fetching..."

#define LoginClass @"User"
#define LoginEntity @""
#define LoginKey @"User"
#define LoginMsg @"Login..."

#define AllClubMatchesClass @"ClubMatchesList"
#define AllClubMatchesEntity @""
#define AllClubMatchesKey @"ClubMatchesList"
#define AllClubMatchesMsg @"Loading..."

#define CheckScoreCorrectnessClass @""
#define CheckScoreCorrectnessEntity @""
#define CheckScoreCorrectnessKey @"status"
#define CheckScoreCorrectnessMsg @"Loading..."

#define CalculateMemberRatingsClass @"MemberRatings"
#define CalculateMemberRatingsEntity @""
#define CalculateMemberRatingsKey @"MemberRatings"
#define CalculateMemberRatingsMsg @"Loading..."

#define MemberslistClass @"MemberList"
#define MemberslistEntity @""
#define MemberslistKey @"MemberList"
#define MemberslistMsg @"Fetching..."


#define AllMyMatchesClass @"ClubMatchesList"
#define AllMyMatchesEntity @""
#define AllMyMatchesKey @"MatchesList"
#define AllMyMatchesMsg @"Loading..."

#define MemberId      @"memberid"
#define ClubId      @"clubid"
#define Page      @"page"
#define Limit      @"limit"
#define Gametype      @"gametype"
#define Score     @"score"
#define Member1     @"member1"
#define Member2     @"member2"
#define Result     @"result"
#define Player1     @"player1"
#define Player2     @"player2"
#define match_date     @"match_date"
#define game_1      @"game_1"
#define game_2      @"game_2"
#define game_3      @"game_3"
#define game_4      @"game_4"
#define game_5      @"game_5"
#define game_6      @"game_6"
#define game_7      @"game_7"
#define match_result    @"match_result"
#define match_score     @"match_score"
#define pos_neg_m1      @"pos_neg_m1"
#define rating_change_m1        @"rating_change_m1"
#define ranking_m1      @"ranking_m1"
#define pos_neg_m2      @"pos_neg_m2"
#define rating_change_m2        @"rating_change_m2"
#define ranking_m2      @"ranking_m2"
#define Search @"search"


#define SUCCESS_STATUS      @"status"
#define FAILED_STATUS       @"failed"
#define ERROR_STATUS        @"error"
#define MESSAGE             @"message"
#define STATUS              @"status"
#define WSAuthKey           @"auth"
#define WSClassKey          @"data"
#define WSErrorMsg          @"Unable to get response"
#define WSAdminConfig       @"adminConfig"
#define WSGlobalPassword    @"globalPassword"
#define WSUserAgent         @"userAgent"
#define WSTempToken         @"tempToken"
#define WSUserToken         @"userToken"
#define SMSMODE_NEXMO       @"Nexmo"
#define SMSMODE_OTHER       @"other"
#define STATUS_FOLLOW       @"FOLLOW"
#define STATUS_UNFOLLOW     @"UNFOLLOW"
#define STATUS_PENDING      @"PENDING"
#define STATUS_HIDE         @"HIDE"
#define STATUS_NONE         @"NONE"

#define WSdevice_token @"device_token"
#define WSdevice_type @"device_type"
#define WSemail  @"email"
#define Wsfacebook_id @"facebook_id"
#define Wsfacebook_ids @"facebook_ids"
#define WSaccess_key @"access_key"
#define WSsecret_key @"secret_key"
#define WSpassword @"password"
#define WSis_testdata @"is_testdata"
#define WSprofile_picture @"profile_picture"
#define WSskin_type @"skin_type"
#define WSskin_tone @"skin_tone"
#define WScontact_number @"contact_number"
#define WScountry_code @"country_code"
#define WSlast_name @"last_name"
#define WSfirst_name @"first_name"
#define WSuser_name @"user_name"
#define WSuser_id @"user_id"
#define WSis_email @"is_email"
#define WSverification_code @"verification_code"
#define WSsms_mode @"sms_mode"
#define WSfollower_id @"follower_id"
#define WSfriend_id @"friend_id"
#define WStitle @"title"
#define WScategory_id @"category_id"
#define WSdescription @"description"
#define WStagged_text @"tagged_text"
#define WSstatus @"status"
#define WSmessage @"message"
#define WSno_of_upload_image @"no_of_upload_image"
#define WSremoval_image_id @"removal_image_id"
#define WSnote_images @"note_images"
#define Wsorder @"order"
#define WSstart_limit @"start_limit"
#define WSend_limit @"end_limit"
#define WSnote_id @"note_id"
#define WScollection_id @"collection_id"
#define WScomment_text @"comment_text"
//====================================




#endif /* WSConstant_h */
