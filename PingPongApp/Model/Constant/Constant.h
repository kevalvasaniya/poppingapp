//
//  Constant.h
//  djawab
//
//  Created by C218 on 25/08/16.
//  Copyright © 2016 C218. All rights reserved.
//



#ifndef Constant_h
#define Constant_h

#define DEBUG_LOGS 1

#if DEBUG_LOGS
#define NSLog(...) NSLog(__VA_ARGS__)
#else
#define NSLog(...)
#endif

#define USER_REG_EXP @"^[A-Za-z0-9_-]{3,15}$"
#define EMAIL_REG_EXP @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,5}"


#define iPadDevice                  UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad
#define iPhoneDevice                UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone
#define isRetina                    ([[UIScreen mainScreen] scale] == 2.0)


#define AppContext ((AppDelegate*)[[UIApplication sharedApplication]delegate])

#define Set_Local_Image(ImageName)      [UIImage imageNamed:[NSString stringWithFormat:@"%@", ImageName]]

#define getStoryboard(StoryboardWithName) [UIStoryboard storyboardWithName:[NSString stringWithFormat:@"%@%@",StoryboardWithName, iPadDevice ? @"iPad" : @""] bundle:NULL]
#define loadViewController(StoryBoardName, VCIdentifer) [getStoryboard(StoryBoardName)instantiateViewControllerWithIdentifier:VCIdentifer]

 #define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

#define NULL_TO_NIL(obj) ({ __typeof__ (obj) __obj = (obj); __obj == [NSNull null] ? nil : obj; })


#define APP_NAME    @"Beauty Journal"
#define TEMP_DEVICETOKEN @"exYx0PPQ4To:APA91bF7isbLCyhJ04EGv5qKwSyFMXs6gXhbtE0wONr3Styj8EexVPQL9PD3GiyWSTb3XGY-8xHXiJlAVJ1veSaJhZtbOz-WTJ9Q1_oFlN6G6lVeF1-BS135wtBLqCrQ-P6TGCHMy2-b"


//fonts
#define THEME_FONT(Size)                          [UIFont fontWithName:@"Roboto-Regular" size:Size]
#define THEME_FONT_LIGHT(Size)                [UIFont fontWithName:@"Roboto-Light" size:Size]
#define THEME_FONT_BOLD(Size)                 [UIFont fontWithName:@"Roboto-Bold" size:Size]
//#define THEME_FONT_MEDIUM(Size)           [UIFont fontWithName:@"Lato-Medium" size:Size]



//colors
#define SELECTED_SIDE_MENU [UIColor colorWithRed:26.0f/255.0f green:87.0f/255.0f blue:156.0f/255.0f alpha:0.0]


#define CLEAR_COLOR [UIColor clearColor]
#define BLACK_COLOR [UIColor blackColor]
#define WHITE_COLOR [UIColor whiteColor]
#define LIGHT_GRAY_COLOR [UIColor lightGrayColor]
#define THEMECOLOR RGB(228,54,69)

#define COLOR_1 RGB(244,178,56)
#define COLOR_2 RGB(255,152,49)
#define COLOR_3 RGB(255,72,30)

#define LIGHT_GRAY_COLOR_WITH_3ALPHA [UIColor colorWithRed:170.0f/255.0f green:170.0f/255.0f blue:170.0f/255.0f alpha:0.30]

#define NAVIGATION_BACK_COLOR [UIColor colorWithRed:250.0f/255.0f green:117.0f/255.0f blue:10.0f/255.0f alpha:1]
#define APP_DARK_GRAY_COLOR [UIColor colorWithRed:91.0f/255.0f green:91.0f/255.0f blue:91.0f/255.0f alpha:1]

#define RGB(R, G, B)            [UIColor colorWithRed:R/255.0 green:G/255.0 blue:B/255.0 alpha:1.0]
#define RGBA(R, G, B, A)            [UIColor colorWithRed:R/255.0 green:G/255.0 blue:B/255.0 alpha:A]

#define APP_BLACK_COLOR  [UIColor colorWithRed:17.0f/255.0f green:24.0f/255.0f blue:30.0f/255.0f alpha:1.0f]
#define APP_COLOR  [UIColor colorWithRed:255/255.0f green:160/255.0f blue:0/255.0f alpha:1.0f]
#define APP_GRAY_COLOR  [UIColor colorWithRed:151.0f/255.0f green:147.0f/255.0f blue:143.0f/255.0f alpha:1.0f]

#define IOS_OLDER_THAN_X(XX)            ( [ [ [ UIDevice currentDevice ] systemVersion ] floatValue ] < XX )
#define IOS_NEWER_OR_EQUAL_TO_X(XX)    ( [ [ [ UIDevice currentDevice ] systemVersion ] floatValue ] >= XX )

#if __IPHONE_OS_VERSION_MIN_REQUIRED >= 80000

#define SCREEN_WIDTH                ((([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) || ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)) ? [[UIApplication sharedApplication].keyWindow bounds].size.width : [[UIApplication sharedApplication].keyWindow bounds].size.height)

#define SCREEN_HEIGHT               ((([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) || ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)) ? [[UIApplication sharedApplication].keyWindow bounds].size.height : [[UIApplication sharedApplication].keyWindow bounds].size.width)

#else

#define SCREEN_WIDTH                ((([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) || ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)) ? [[UIScreen mainScreen] bounds].size.width : [[UIScreen mainScreen] bounds].size.height)

#define SCREEN_HEIGHT               ((([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) || ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)) ? [[UIScreen mainScreen] bounds].size.height : [[UIScreen mainScreen] bounds].size.width)

#endif



//Storyboard

#define StoryBoard_Main   @"Main"
#define StoryBoard_Home   @"Home"
#define StoryBoard_InternalMatches   @"InternamMatches"


#define kIDHomeVC @"idHomeVC"
#define kIDNavigationVC @"idNavigationVC"
#define kIDSidebarVC @"idSidebarVC"
#define kIDLoginVC @"idLoginVC"
#define kIDidInternalMatchVC @"idInternalMatchVC"
#define kIDAddMatchesVC @"idAddMatchesVC"
#define kIDAllClubMatchesVC @"idAllClubMatchesVC"
#define kIDMyMatchesVC @"idMyMatchesVC"
#define kIDDropDownMenu @"DropDownMenu"



//Default Key
#define kUserID @"id"
#define kClubID @"club_id"
#define kFirstName @"first_name"
#define kLastName @"last_name"
#define kPhoto @"photo"
#define kclub_logo @"club_logo"



#define kLogIn @"Already LogIn"
#define kUserObject @"UserObject"
#define kUserEmailID @"User Email"
#define kUID @"UserID"
#define kTokenKey @"TOKEN"
#define kAccessKey @"access_key"
#define kSecretKey @"secret_key"
#define kGlobalPassword @"GlobalPassword"
#define kNoUsername @"nousername"
#define kTempToken @"TempToken"
#define kUserToken @"UserToken"
#define kFacebookId @"FacebookId"
#define kVerificationCode @"VerificationCode"
#define kUserGUID @"UserGUID"
#define kDeviceToken @"DeviceToken"




#endif /* Constant_h */
