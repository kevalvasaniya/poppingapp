//
//  Member1.m
//
//  Created by   on 12/08/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "Member1.h"


NSString *const kMember1Ranking = @"ranking";
NSString *const kMember1String = @"string";
NSString *const kMember1RatingChange = @"rating_change";
NSString *const kMember1PosNeg = @"pos_neg";


@interface Member1 ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation Member1

@synthesize ranking = _ranking;
@synthesize string = _string;
@synthesize ratingChange = _ratingChange;
@synthesize posNeg = _posNeg;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.ranking = [[self objectOrNilForKey:kMember1Ranking fromDictionary:dict] doubleValue];
            self.string = [self objectOrNilForKey:kMember1String fromDictionary:dict];
            self.ratingChange = [self objectOrNilForKey:kMember1RatingChange fromDictionary:dict];
            self.posNeg = [self objectOrNilForKey:kMember1PosNeg fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.ranking] forKey:kMember1Ranking];
    [mutableDict setValue:self.string forKey:kMember1String];
    [mutableDict setValue:self.ratingChange forKey:kMember1RatingChange];
    [mutableDict setValue:self.posNeg forKey:kMember1PosNeg];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.ranking = [aDecoder decodeDoubleForKey:kMember1Ranking];
    self.string = [aDecoder decodeObjectForKey:kMember1String];
    self.ratingChange = [aDecoder decodeObjectForKey:kMember1RatingChange];
    self.posNeg = [aDecoder decodeObjectForKey:kMember1PosNeg];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_ranking forKey:kMember1Ranking];
    [aCoder encodeObject:_string forKey:kMember1String];
    [aCoder encodeObject:_ratingChange forKey:kMember1RatingChange];
    [aCoder encodeObject:_posNeg forKey:kMember1PosNeg];
}

- (id)copyWithZone:(NSZone *)zone {
    Member1 *copy = [[Member1 alloc] init];
    
    
    
    if (copy) {

        copy.ranking = self.ranking;
        copy.string = [self.string copyWithZone:zone];
        copy.ratingChange = [self.ratingChange copyWithZone:zone];
        copy.posNeg = [self.posNeg copyWithZone:zone];
    }
    
    return copy;
}


@end
