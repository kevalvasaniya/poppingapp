//
//  MemberRatings.m
//
//  Created by   on 12/08/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "MemberRatings.h"
#import "Member1.h"
#import "Member2.h"


NSString *const kMemberRatingsMember1 = @"member1";
NSString *const kMemberRatingsMember2 = @"member2";


@interface MemberRatings ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation MemberRatings

@synthesize member1 = _member1;
@synthesize member2 = _member2;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.member1 = [Member1 modelObjectWithDictionary:[dict objectForKey:kMemberRatingsMember1]];
            self.member2 = [Member2 modelObjectWithDictionary:[dict objectForKey:kMemberRatingsMember2]];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[self.member1 dictionaryRepresentation] forKey:kMemberRatingsMember1];
    [mutableDict setValue:[self.member2 dictionaryRepresentation] forKey:kMemberRatingsMember2];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.member1 = [aDecoder decodeObjectForKey:kMemberRatingsMember1];
    self.member2 = [aDecoder decodeObjectForKey:kMemberRatingsMember2];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_member1 forKey:kMemberRatingsMember1];
    [aCoder encodeObject:_member2 forKey:kMemberRatingsMember2];
}

- (id)copyWithZone:(NSZone *)zone {
    MemberRatings *copy = [[MemberRatings alloc] init];
    
    
    
    if (copy) {

        copy.member1 = [self.member1 copyWithZone:zone];
        copy.member2 = [self.member2 copyWithZone:zone];
    }
    
    return copy;
}


@end
