//
//  MemberRatings.h
//
//  Created by   on 12/08/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Member1, Member2;

@interface MemberRatings : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) Member1 *member1;
@property (nonatomic, strong) Member2 *member2;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
