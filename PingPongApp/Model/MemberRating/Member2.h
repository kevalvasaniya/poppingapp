//
//  Member2.h
//
//  Created by   on 12/08/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface Member2 : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double ranking;
@property (nonatomic, strong) NSString *string;
@property (nonatomic, strong) NSString *ratingChange;
@property (nonatomic, strong) NSString *posNeg;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
