//
//  Member2.m
//
//  Created by   on 12/08/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "Member2.h"


NSString *const kMember2Ranking = @"ranking";
NSString *const kMember2String = @"string";
NSString *const kMember2RatingChange = @"rating_change";
NSString *const kMember2PosNeg = @"pos_neg";


@interface Member2 ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation Member2

@synthesize ranking = _ranking;
@synthesize string = _string;
@synthesize ratingChange = _ratingChange;
@synthesize posNeg = _posNeg;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.ranking = [[self objectOrNilForKey:kMember2Ranking fromDictionary:dict] doubleValue];
            self.string = [self objectOrNilForKey:kMember2String fromDictionary:dict];
            self.ratingChange = [self objectOrNilForKey:kMember2RatingChange fromDictionary:dict];
            self.posNeg = [self objectOrNilForKey:kMember2PosNeg fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.ranking] forKey:kMember2Ranking];
    [mutableDict setValue:self.string forKey:kMember2String];
    [mutableDict setValue:self.ratingChange forKey:kMember2RatingChange];
    [mutableDict setValue:self.posNeg forKey:kMember2PosNeg];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.ranking = [aDecoder decodeDoubleForKey:kMember2Ranking];
    self.string = [aDecoder decodeObjectForKey:kMember2String];
    self.ratingChange = [aDecoder decodeObjectForKey:kMember2RatingChange];
    self.posNeg = [aDecoder decodeObjectForKey:kMember2PosNeg];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_ranking forKey:kMember2Ranking];
    [aCoder encodeObject:_string forKey:kMember2String];
    [aCoder encodeObject:_ratingChange forKey:kMember2RatingChange];
    [aCoder encodeObject:_posNeg forKey:kMember2PosNeg];
}

- (id)copyWithZone:(NSZone *)zone {
    Member2 *copy = [[Member2 alloc] init];
    
    
    
    if (copy) {

        copy.ranking = self.ranking;
        copy.string = [self.string copyWithZone:zone];
        copy.ratingChange = [self.ratingChange copyWithZone:zone];
        copy.posNeg = [self.posNeg copyWithZone:zone];
    }
    
    return copy;
}


@end
