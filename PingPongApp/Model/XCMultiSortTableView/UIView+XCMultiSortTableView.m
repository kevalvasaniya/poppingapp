//
//  UIView+XCMultiTableView.m
//  XCMultiTableDemo
//
//  Created by Kingiol on 13-7-22.
//  Copyright (c) 2013年 Kingiol. All rights reserved.
//

#import "UIView+XCMultiSortTableView.h"

@implementation UIView (XCMultiTableView)

- (void)addBottomLineWithWidth:(CGFloat)width bgColor:(UIColor *)color {
    CGRect f = self.frame;
    f.size.height += width;
    self.frame = f;
    
    UIView *bottomLine = [[UIView alloc] initWithFrame:CGRectMake(0.0, self.frame.size.height - width, self.frame.size.width, width)];
    bottomLine.backgroundColor = color;
    //bottomLine.backgroundColor = [self colorWithHexString:@"979A99" alpha:1];
    bottomLine.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
    [self addSubview:bottomLine];
}

- (UIView *)addVerticalLineWithWidth:(CGFloat)width bgColor:(UIColor *)color atX:(CGFloat)x {
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(x, 0.0f, width, self.bounds.size.height)];
    line.backgroundColor = color;
    //line.backgroundColor = [self colorWithHexString:@"979A99" alpha:1];
    line.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleRightMargin;
    [self addSubview:line];
    return line;
}

#pragma mark - Hext to color
- (UIColor *)colorWithHexString:(NSString *)str_HEX  alpha:(CGFloat)alpha_range{
    int red = 0;
    int green = 0;
    int blue = 0;
    sscanf([str_HEX UTF8String], "#%02X%02X%02X", &red, &green, &blue);
    return  [UIColor colorWithRed:red/255.0 green:green/255.0 blue:blue/255.0 alpha:alpha_range];
}

@end
