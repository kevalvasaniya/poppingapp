//
//  MemberList.m
//
//  Created by   on 08/08/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "MemberList.h"


NSString *const kMemberListMemberId = @"member_id";
NSString *const kMemberListName = @"name";
NSString *const kMemberListMemberEmail = @"member_email";
NSString *const kMemberListUnionNumber = @"union_number";


@interface MemberList ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation MemberList

@synthesize memberId = _memberId;
@synthesize name = _name;
@synthesize memberEmail = _memberEmail;
@synthesize unionNumber = _unionNumber;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.memberId = [self objectOrNilForKey:kMemberListMemberId fromDictionary:dict];
            self.name = [self objectOrNilForKey:kMemberListName fromDictionary:dict];
            self.memberEmail = [self objectOrNilForKey:kMemberListMemberEmail fromDictionary:dict];
            self.unionNumber = [self objectOrNilForKey:kMemberListUnionNumber fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.memberId forKey:kMemberListMemberId];
    [mutableDict setValue:self.name forKey:kMemberListName];
    [mutableDict setValue:self.memberEmail forKey:kMemberListMemberEmail];
    [mutableDict setValue:self.unionNumber forKey:kMemberListUnionNumber];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.memberId = [aDecoder decodeObjectForKey:kMemberListMemberId];
    self.name = [aDecoder decodeObjectForKey:kMemberListName];
    self.memberEmail = [aDecoder decodeObjectForKey:kMemberListMemberEmail];
    self.unionNumber = [aDecoder decodeObjectForKey:kMemberListUnionNumber];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_memberId forKey:kMemberListMemberId];
    [aCoder encodeObject:_name forKey:kMemberListName];
    [aCoder encodeObject:_memberEmail forKey:kMemberListMemberEmail];
    [aCoder encodeObject:_unionNumber forKey:kMemberListUnionNumber];
}

- (id)copyWithZone:(NSZone *)zone {
    MemberList *copy = [[MemberList alloc] init];
    
    
    
    if (copy) {

        copy.memberId = [self.memberId copyWithZone:zone];
        copy.name = [self.name copyWithZone:zone];
        copy.memberEmail = [self.memberEmail copyWithZone:zone];
        copy.unionNumber = [self.unionNumber copyWithZone:zone];
    }
    
    return copy;
}


@end
