//
//  ClubMatchesList.m
//
//  Created by   on 07/08/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "ClubMatchesList.h"


NSString *const kClubMatchesListMatchResult = @"match_result";
NSString *const kClubMatchesListRating = @"rating";
NSString *const kClubMatchesListPlayer2 = @"Player2";
NSString *const kClubMatchesListMatchDate = @"match_date";
NSString *const kClubMatchesListInternalMatchId = @"internal_match_id";
NSString *const kClubMatchesListPlayer1 = @"Player1";


@interface ClubMatchesList ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation ClubMatchesList

@synthesize matchResult = _matchResult;
@synthesize rating = _rating;
@synthesize player2 = _player2;
@synthesize matchDate = _matchDate;
@synthesize internalMatchId = _internalMatchId;
@synthesize player1 = _player1;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.matchResult = [self objectOrNilForKey:kClubMatchesListMatchResult fromDictionary:dict];
            self.rating = [self objectOrNilForKey:kClubMatchesListRating fromDictionary:dict];
            self.player2 = [self objectOrNilForKey:kClubMatchesListPlayer2 fromDictionary:dict];
            self.matchDate = [self objectOrNilForKey:kClubMatchesListMatchDate fromDictionary:dict];
            self.internalMatchId = [self objectOrNilForKey:kClubMatchesListInternalMatchId fromDictionary:dict];
            self.player1 = [self objectOrNilForKey:kClubMatchesListPlayer1 fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.matchResult forKey:kClubMatchesListMatchResult];
    [mutableDict setValue:self.rating forKey:kClubMatchesListRating];
    [mutableDict setValue:self.player2 forKey:kClubMatchesListPlayer2];
    [mutableDict setValue:self.matchDate forKey:kClubMatchesListMatchDate];
    [mutableDict setValue:self.internalMatchId forKey:kClubMatchesListInternalMatchId];
    [mutableDict setValue:self.player1 forKey:kClubMatchesListPlayer1];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.matchResult = [aDecoder decodeObjectForKey:kClubMatchesListMatchResult];
    self.rating = [aDecoder decodeObjectForKey:kClubMatchesListRating];
    self.player2 = [aDecoder decodeObjectForKey:kClubMatchesListPlayer2];
    self.matchDate = [aDecoder decodeObjectForKey:kClubMatchesListMatchDate];
    self.internalMatchId = [aDecoder decodeObjectForKey:kClubMatchesListInternalMatchId];
    self.player1 = [aDecoder decodeObjectForKey:kClubMatchesListPlayer1];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_matchResult forKey:kClubMatchesListMatchResult];
    [aCoder encodeObject:_rating forKey:kClubMatchesListRating];
    [aCoder encodeObject:_player2 forKey:kClubMatchesListPlayer2];
    [aCoder encodeObject:_matchDate forKey:kClubMatchesListMatchDate];
    [aCoder encodeObject:_internalMatchId forKey:kClubMatchesListInternalMatchId];
    [aCoder encodeObject:_player1 forKey:kClubMatchesListPlayer1];
}

- (id)copyWithZone:(NSZone *)zone {
    ClubMatchesList *copy = [[ClubMatchesList alloc] init];
    
    
    
    if (copy) {

        copy.matchResult = [self.matchResult copyWithZone:zone];
        copy.rating = [self.rating copyWithZone:zone];
        copy.player2 = [self.player2 copyWithZone:zone];
        copy.matchDate = [self.matchDate copyWithZone:zone];
        copy.internalMatchId = [self.internalMatchId copyWithZone:zone];
        copy.player1 = [self.player1 copyWithZone:zone];
    }
    
    return copy;
}


@end
