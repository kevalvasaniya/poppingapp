//
//  ClubMatchesList.h
//
//  Created by   on 07/08/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface ClubMatchesList : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *matchResult;
@property (nonatomic, strong) NSString *rating;
@property (nonatomic, strong) NSString *player2;
@property (nonatomic, strong) NSString *matchDate;
@property (nonatomic, strong) NSString *internalMatchId;
@property (nonatomic, strong) NSString *player1;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
